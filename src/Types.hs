{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}

{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TemplateHaskell #-}

{-# LANGUAGE DeriveGeneric #-}


module Types where

import Data.Text (Text, unpack)

import Database.Persist
import Database.Persist.Postgresql
import Database.Persist.TH
import Text.RawString.QQ (r)

import Data.Time.Calendar (Day(..))
import Data.Time.Format (parseTimeM, defaultTimeLocale)

import Data.Maybe (fromMaybe, isJust)
import qualified Data.ByteString.Char8 as B (unpack, filter, map)
import Data.List (find)

import PropertyType (PropertyType(..), RepworkRule(..))

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
ItemType
    name            Text
    group           Text
    uom             Text
    descrTmpl       Text
    recuperation    Double default=0
    planningMethod  Text default=''
    localPurchase   Bool default=False
    customsCode     Text default=''
    customsDuty     Double default=0
    note            Text default=''
    repworkRule     RepworkRule Maybe
    ITypeName       name
    deriving        Show Eq

PlanItem json
    itemType    ItemTypeId
    name        Text
    price       Double default=0
    PItemName   name
    deriving    Show Eq

Bomtmpl
    parenttype          ItemTypeId
    name                Text
    BomtmplNameConstr   name

Item json
    code1C          Int
    pn              Text            default=''
    description     Text
    uom             Text
    price           Double
    packageSize     Double          default=0
    planItem        PlanItemId
    conversion      Double          default=1
    isMain          Bool            default=False
    isObsolete      Bool            default=False
    bomtmpl         BomtmplId       Maybe
    Code1C code1C
    deriving    Show Eq

Bom json
    parent      PlanItemId
    child       PlanItemId
    qty         Double
    ParentChild parent child

StockUpload
    date        Day
    creatorIp   Text
    Day date

OnOrder
    upload      StockUploadId
    rfq         Text
    date        Day
    item        ItemId
    qty         Double
    price       Double
    eta         Day
    deriving    Show

OnHand
    upload      StockUploadId
    item        ItemId
    placeType   Text
    placeName   Text
    ubication   Text
    owner       Text
    qty         Double
    status1c    Text
    status      Text
    deriving    Show

ItemProperty
    item            ItemId
    propertyType    PropertyType
    value           Text
    ItemPropertyUnique item propertyType

Bomlinetmpl
    tmpl            BomtmplId
    posOnDrawing    Text
    childtype       ItemTypeId
    qty             Text
    propertyType    PropertyType    Maybe
    comment         Text            Maybe
    TmplPosOnDrawingUnique tmpl posOnDrawing 
    deriving        Eq

Propertyfilter
    tmplLine        BomlinetmplId
    propertyType    PropertyType
    value           Text
    TmpllinePropertyUnique tmplLine propertyType

Reworkfilter
    itemType        ItemTypeId
    propertyType    PropertyType
    valuerf         Text
    TypePropertyRFUnique itemType propertyType

ItemBomLink
    item            ItemId
    bomline         BomlinetmplId
    ItemBomLinkUnique item bomline
|]


data BomFromTmpl = BomFromTmpl
    { lineType :: Entity ItemType
    , bomlineTmpl ::  Entity Bomlinetmpl
    , evalQty :: Int
    , filtersParsed :: [(PropertyType, Text)]
    , items :: [Entity Item]
    }


uoms2 :: [(Text, Text)]
uoms2 = [("ea","ea"), ("ft", "ft"), ("m", "m"), ("kg", "kg"), ("gl", "gl"), ("mm", "mm")]

uoms :: [(String, String)]
uoms = [("ea","ea"), ("ft", "ft"), ("m", "m"), ("kg", "kg"), ("gl", "gl"), ("mm", "mm")]

groups :: [(String, String)]
groups = [("Pump","1"), ("Pump-parts", "2"), ("GS/Intake", "3"), ("GS/Intake-parts", "4")
            , ("Seal", "5"), ("Seal-parts", "6"), ("Motor", "7"), ("Motor-parts", "8")
            , ("Cable", "9"), ("Accessories", "10"), ("Consumables", "11"), ("Sensor", "12")
            , ("Other", "13"), ("SF", "14"), ("Tools", "15")]

getGroupName :: Text -> String
getGroupName groupId =
    fromMaybe "Error" $ fmap fst $ Data.List.find (\(_, id) -> Data.Text.unpack groupId == id) groups


type RepworkRuleFunc = [(PropertyType, Text)] -> [(PropertyType, Text)] -> Bool

repworkRuleFuncSelector :: Maybe RepworkRule -> RepworkRuleFunc
repworkRuleFuncSelector mRepworkRule =
    case mRepworkRule of
        Just CorrosionResistenceRR      -> corrosionResistenceRRfunc
        Just CutLengthMMRR              -> cutLengthMMRRfunc
        _                               -> alwaysFalseRRfunc

alwaysFalseRRfunc :: RepworkRuleFunc
alwaysFalseRRfunc _ _ = False


corrResRating :: Text -> Int
corrResRating val =
    case val of
        "CS"    ->  1
        "SS"    ->  5
        "Monel" ->  7
        _       ->  999


corrosionResistenceRRfunc :: RepworkRuleFunc
corrosionResistenceRRfunc goal candidate = all checkProperty goal
    where
        checkProperty (Material, goalValue) =
            isJust $ find
                (\(propType, candValue) ->
                    (propType == Material)
                    &&
                    (corrResRating candValue >= corrResRating goalValue))
                candidate
        checkProperty goalProp = any (\candProp -> candProp == goalProp) candidate


cutLengthMMRRfunc :: RepworkRuleFunc
cutLengthMMRRfunc goal candidate = all checkProperty goal
    where
        checkProperty (LengthMM, goalValue) =
            isJust $ find
                (\(propType, candValue) ->
                    (propType == LengthMM)
                    && 
                    ((read $ unpack candValue :: Float) > (read $ unpack goalValue :: Float)))
                candidate
        checkProperty goalProp = any (\candProp -> candProp == goalProp) candidate


