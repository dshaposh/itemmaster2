{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Csv where

import Text.RawString.QQ (r)
import GHC.Generics (Generic)
import Data.Text (Text)
import Data.Char (ord)

import Data.Time.Calendar (Day(..))
import Data.Time.Format (parseTimeM, defaultTimeLocale)

import Data.Text.Encoding (decodeLatin1)
import Data.Char (isSpace)
import qualified Data.ByteString.Char8 as B (unpack, filter, map, dropWhile)
import Data.Csv (FromNamedRecord, FromField(..), DecodeOptions(..),Parser, (.:), parseNamedRecord)

class WithCode1c a where
    code1c :: a -> Int


data OnOrder = OnOrder
    { rfq                   :: Text
    , rfqDate               :: Day
    , code1cOnOrder         :: Int
    , pnOnOrder             :: Text
    , iDescriptionOnOrder   :: Text
    , uomOnOrder            :: Text
    , qtyOnOrder            :: Double1c
    , price                 :: Double1c
    , eta                   :: Day
    } deriving (Generic, Show)

instance FromNamedRecord OnOrder where
    parseNamedRecord m = OnOrder
        <$> m .: "rfq"
        <*> m .: "date"
        <*> m .: "code1c"
        <*> m .: "pn" 
        <*> fmap decodeLatin1 (m .: "description")
        <*> m .: "uom"
        <*> m .: "qty"
        <*> m .: "price"
        <*> m .: "eta"
instance WithCode1c OnOrder where
    code1c = code1cOnOrder

data OnHand = OnHand
    { code1cOnHand          :: Int
    , pnOnHand              :: Text
    , iDescriptionOnHand    :: Text
    , uomOnHand             :: Text
--    , placeType             :: Text
    , placeName             :: Text
    , ubication             :: Text
    , owner                 :: Text
    , status1c              :: Text
    , status                :: Status
    , qtyOnHand             :: Double1c
    } deriving (Generic, Show)

instance FromNamedRecord OnHand where
    parseNamedRecord m = OnHand
        <$> m .: "code1c"
        <*> m .: "pn"
        <*> fmap decodeLatin1 (m .: "description")
        <*> m .: "uom"
        <*> m .: "place"
        <*> m .: "ubication"
        <*> m .: "owner"
        <*> m .: "status"
        <*> m .: "status"
        <*> m .: "qty"

instance WithCode1c OnHand where
    code1c = code1cOnHand

instance FromField Day where
    parseField f = case mDate of
        Nothing -> fail ("Failed"::String)
        Just date -> pure date
        where mDate = parseTimeM True defaultTimeLocale "%d.%m.%Y" $ B.unpack f

newtype Double1c = Double1c { getDouble :: Double }
newtype Status = Status { getStatus :: Text }

instance FromField Double1c where
    parseField f = Double1c <$> ((parseField (sanitizedField f)) :: Parser Double) 
        where
            sanitizedField f = B.map comma2dot $ B.filter (\c -> c /= '.') f
            comma2dot ',' = '.'
            comma2dot x = x
instance Show Double1c where
    show = show . getDouble

instance FromField Status where
    parseField f 
        | f == "New tested" = pure $ Status "Ready"
        | f == "New untested" = pure $ Status "Ready"
        | f == "Repaired" = pure $ Status "Ready"
        | f == "To be inspected" = pure $ Status "Used"
        | f == "To be repaired" = pure $ Status  "Used"
        | f == "Reusable" = pure $ Status  "Ready"
        | f == "Written-off" = pure $ Status  "Omit"
        | f == "Scrap" = pure $ Status  "Omit"
        | B.dropWhile isSpace f == "" = pure $ Status  "Ready"
instance Show Status where
    show = show . getStatus


decodeOptions :: DecodeOptions
decodeOptions = DecodeOptions
    { decDelimiter = fromIntegral (ord ';')
    }

codes1cLookupSql :: Text
codes1cLookupSql= [r|
    SELECT i.code1_c, i.id, it.group 
    FROM item i
    INNER JOIN plan_item pi
    ON i.plan_item = pi.id
    INNER JOIN item_type it
    ON pi.item_type = it.id
|]
