{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module ViewItems where

import Text.RawString.QQ (r)
import GHC.Int
import Data.Monoid ((<>))
import Control.Applicative ((<|>))
import Control.Monad.IO.Class (liftIO)
import Data.List (groupBy, group, head, sort, find)
import Data.Maybe (fromMaybe, isJust, catMaybes)
import qualified Data.Text as T
import Data.Double.Conversion.Text (toFixed, toShortest)
import Data.Monoid (mempty)
import Lucid.Base
import Lucid.Html5
import Database.Persist.Postgresql

import PropertyType
import Types
import qualified ViewsLucid as VL
import Database.Persist.Sql (ConnectionPool)


editProperties :: Entity ItemType -> PropertyType -> ConnectionPool -> HtmlT IO ()
editProperties itemType propertyType pool = VL.templatePage2 body
    where
        body = do
            let filterVal = toPersistValue $ fromSqlKey $ entityKey itemType 
            itemsWithPropsValues :: [(Entity Item, Maybe (Entity ItemProperty), Maybe (Entity Bomtmpl))]
                <- liftIO $ runSqlPool (rawSql itemsPropertiesSql [filterVal]) pool

            let items = groupBy (\(item1, _, _) (item2, _, _) -> item1 == item2) itemsWithPropsValues

            h1_ $ do
                a_ [ href_ ("/group?id=" <> (itemTypeGroup $ entityVal itemType)) ]
                    $ toHtml (getGroupName $ itemTypeGroup $ entityVal itemType)
                toHtml (" // "::T.Text)
                toHtml $ (itemTypeName . entityVal) itemType
            h1_ "Edit property"
            div_ $ toHtml $ "UOM: " <> (itemTypeUom $ entityVal  itemType)
            form_ [ method_ "POST", action_ "/editProperties" ] $ do       
                input_ [ type_ "hidden", name_ "property", value_  (T.pack $ show $ propertyType)]
                input_ [ type_ "hidden", name_ "type", value_  (T.pack $ show $ fromSqlKey $ entityKey itemType)]
                div_ [ class_ "buttons"] $ do
                    button_ [ type_ "submit" ] "Submit"
                    button_ [ type_ "reset" ] "Reset"
                table_ $ do
                    tr_ $ do 
                        th_ "id"
                        th_ "Code1C"
                        th_ "PN"
                        th_ "Description"
                        th_ "UOM"
                        th_ $ toHtml $ T.pack $ show propertyType
                    (mapM_ itemLine items)

                where
                    itemLine block = tr_ $ do
                        let (item, _, _) = Prelude.head block
                            itemKey = T.pack $ show $ fromSqlKey $ entityKey item
                            propertyValue = find
                                (\el -> (itemPropertyPropertyType $ entityVal el) == propertyType)
                                $ catMaybes
                                $ fmap (\(_, x, _) -> x) block
                            propertyValueKey = maybe "" (T.pack . show . fromSqlKey . entityKey) propertyValue
                        td_ $ (toHtml . show . fromSqlKey . entityKey) item
                        td_ $ (toHtml . show . itemCode1C . entityVal) item 
                        td_ $ (toHtml . itemPn . entityVal) item
                        td_ $ toHtml $ itemDescription $ entityVal item
                        td_ $ toHtml $ itemUom $ entityVal item
                        td_ $ input_
                            [ name_ itemKey
                            , value_ (maybe "" (itemPropertyValue . entityVal) propertyValue)
                            ]
                        toHtml (""::T.Text)


items :: Entity ItemType -> Bool -> ConnectionPool -> HtmlT IO ()
items itemType isAuth pool = VL.templatePage2 body
    where
        body = do

            let filterVal = toPersistValue $ fromSqlKey $ entityKey itemType 

            itemsWithPropsValues :: [(Entity Item, Maybe (Entity ItemProperty), Maybe (Entity Bomtmpl))]
                <- liftIO $ runSqlPool (rawSql itemsPropertiesSql [filterVal]) pool

            let props = fmap head
                            $ group
                            $ sort
                            $ fmap (\(_, Just p, _) -> (itemPropertyPropertyType . entityVal) p)
                            $ filter (\(_, m, _) -> isJust m) itemsWithPropsValues

            let itemLine block = with (tr_ $ do 
                    td_ $ if isAuth
                            then a_ [ href_ ("/aditItem?id=" <> key) ] $ toHtml key
                            else toHtml key 
                    td_ $ (toHtml . show . itemCode1C . entityVal) item 
                    td_ $ (toHtml . itemPn . entityVal) item 
                    td_ $ (toHtml . itemDescription . entityVal) item 
                    td_ $ 
                        maybe
                            ""
                            (\bom -> a_ [href_ $ "/generateBom?item=" <> key] $ toHtml $ bomtmplName $ entityVal bom)
                            mBom 
                    if isAuth
                        then td_ $ (toHtml . (toFixed 2) . itemPrice . entityVal) item 
                        else mempty
                    td_ $ a_ [href_ $ "/linksToBoms?item=" <> key] "tbd"
                    (mapM_ (\key -> td_ $ toHtml $ propertyValue key) props)
                    if isAuth
                        then td_ $ do
                            button_
                                [ type_ "submit"
                                , formmethod_ "POST"
                                , formaction_ "/deleteItem"
                                , name_ "id"
                                , value_ key
                                ]
                                "D"
                        --else mempty
                        else toHtml (""::T.Text)
                    ) (if (itemIsObsolete $ entityVal item) then [class_ "selected"] else [])

                    where  
                        (item, _, mBom) = Prelude.head block 
                        key = T.pack $ show $ fromSqlKey $ entityKey item
                        propertyValue valueKey = maybe "~" (itemPropertyValue . entityVal)
                                $ find (\pr -> (itemPropertyPropertyType . entityVal) pr == valueKey)
                                $ catMaybes
                                $ fmap (\(_, x, _) -> x) block


            h1_ $ do
                a_ [ href_ ("/group?id=" <> (itemTypeGroup $ entityVal itemType)) ]
                    $ toHtml (getGroupName $ itemTypeGroup $ entityVal itemType)
                toHtml (" // "::T.Text)
                toHtml $ (itemTypeName . entityVal) itemType
            div_ $ toHtml $ "UOM: " <> (itemTypeUom $ entityVal  itemType)
            div_ $ if isAuth
                then a_ [ href_ ("editPlanItem?itype=" <> typeKey)]
                        "Add item"
                else mempty
            form_ $ do       
                table_ $ do
                    tr_ $ do 
                        th_ "id"
                        th_ "Code1C"
                        th_ "PN"
                        th_ "Description"
                        th_ "Bom"
                        (if isAuth then th_ "Price" else mempty)
                        th_ "in Boms #"
                        (mapM_
                            (if isAuth
                                then (\prop -> th_ $ a_
                                    [href_ $ "/editProperties?type=" <> typeKey <> "&property=" <> (T.pack $ show prop) ]
                                    (toHtml $ T.pack $ show prop))
                                else (\prop -> th_ $ toHtml $ T.pack $ show prop))
                            props)
                        (if isAuth then th_ "Action" else mempty)
                    let items = groupBy (\(item1, _, _) (item2, _, _) -> item1 == item2) itemsWithPropsValues
                    tbody_ $ mapM_ itemLine items

                where
                    typeKey = T.pack $ show $ fromSqlKey $ entityKey itemType

item2bomsLinks :: Entity Item -> Bool -> ConnectionPool -> HtmlT IO ()
item2bomsLinks eitem isAuth pool =
    let body = do

            let itemKey = entityKey eitem
                item = entityVal eitem
                planItemKey = itemPlanItem item

            planItem <- liftIO $ runSqlPool (getJust planItemKey) pool
            let itemTypeKey = planItemItemType planItem
            itemType <- liftIO $ runSqlPool (getJust itemTypeKey) pool
            bomLinesAll :: [(Entity Bomtmpl, Entity Bomlinetmpl) ]
                <- liftIO $ runSqlPool (rawSql bomlinesSql [toPersistValue itemTypeKey]) pool

            existingLinks :: [ Entity ItemBomLink ] <- liftIO $ runSqlPool (selectList [ItemBomLinkItem ==. itemKey] []) pool

            let linkLine (ebomtmpl, ebomline) = tr_ $ do
                    td_ $ toHtml $ bomtmplName bomtmpl
                    td_ $ toHtml $ bomlinetmplPosOnDrawing bomline
                    td_ $ toHtml $ bomlinetmplQty bomline
                    td_ $ toHtml $ fromMaybe "" $ bomlinetmplComment bomline
                    td_ $ do
                        input_ $ [name_ bomlineKey, value_ "1", type_ "hidden"] 
                        input_ $ [name_ bomlineKey, value_ "1", type_ "checkbox"] <> (if isChecked then [checked_] else [])
                    toHtml (""::T.Text)
                    where
                        bomtmpl = entityVal ebomtmpl
                        bomline = entityVal ebomline
                        bomlineKey = T.pack $ show $ fromSqlKey $ entityKey ebomline
                        isChecked = maybe False (const True) $ find
                            (\link -> (itemBomLinkBomline $ entityVal link) == entityKey ebomline)
                            existingLinks

            h1_ $ toHtml $ "Item " <> (T.pack $ show $ itemCode1C item) <> "-" <> (itemPn item) <> "-" <> (itemDescription item)
            h1_ "Links to BOMs"
            form_ [method_ "POST"] $ do
                input_ [type_ "hidden", name_ "item", value_ $ T.pack $ show $ fromSqlKey itemKey]
                if isAuth then
                    div_ [ class_ "buttons" ] $ do
                        button_ [type_ "submit"] "Save"
                        button_ [type_ "reset"] "Reset"
                else mempty
                table_ $ do
                    tr_ $ do
                        th_ "BOM name"
                        th_ "Pos on drawing"
                        th_ "Qty"
                        th_ "Comment"
                        th_ "Conversion"
                    (mapM_ linkLine bomLinesAll)
        
    in VL.templatePage2 body

data IniConfAditItemPage
    = EditItem (Entity Item)
    | CreateInPlanItem (Entity PlanItem)
    | Empty

aditItemPage :: Maybe (Key ItemType) -> IniConfAditItemPage -> Bool -> ConnectionPool -> HtmlT IO ()
aditItemPage mItemTypeKey iniConf isAuth pool = VL.templatePage2 body
    where body = do
            itemTypes :: [Entity ItemType]
                <- liftIO $ runSqlPool (selectList [] [Asc ItemTypeGroup, Asc ItemTypeName]) pool

            mIniItemTypeKey <- case iniConf of
                Empty -> return Nothing
                EditItem itemEnt -> fmap (Just . planItemItemType) $ liftIO $ runSqlPool
                                        (getJust $ itemPlanItem $ entityVal itemEnt) pool
                CreateInPlanItem planItemEnt -> return $ Just $ planItemItemType $ entityVal planItemEnt 

            let mItypeKey2show = mItemTypeKey <|> mIniItemTypeKey 

            planItems :: [Entity PlanItem] <- case mItypeKey2show of
                Nothing -> return []
                Just key -> liftIO $ runSqlPool (selectList [PlanItemItemType ==. key][]) pool 

            bomTemplates :: [Entity Bomtmpl] <- case mItypeKey2show of
                Nothing -> return []
                Just key -> liftIO $ runSqlPool (selectList [BomtmplParenttype ==. key][]) pool 

            let mItemId = case iniConf of
                    EditItem itemEnt -> Just $ fromSqlKey $ entityKey itemEnt
                    CreateInPlanItem _ -> Nothing
                    Empty -> Nothing

            let mPlanItemId = case iniConf of
                    EditItem itemEnt -> Just $ fromSqlKey $ itemPlanItem $ entityVal itemEnt
                    CreateInPlanItem planItemEnt -> Just $ fromSqlKey $ entityKey planItemEnt
                    Empty -> Nothing

            let showField fieldAccessor = case iniConf of
                    EditItem itemEnt -> Just $ fieldAccessor $ entityVal itemEnt
                    CreateInPlanItem _ -> Nothing
                    Empty -> Nothing

            let optionId entity = T.pack $ show $ fromSqlKey $ entityKey entity
                itypesOptions = map (\entity -> (itypeDescr entity, optionId entity)) itemTypes
                itypeDescr entity =
                    (T.pack $ getGroupName $ itemTypeGroup $ entityVal entity)
                                <> "-"
                                <> (itemTypeName $ entityVal entity)
                pitemsOptions = map (\pitem -> (planItemName $ entityVal pitem, optionId pitem)) planItems
                bomsOptions = map bomOption bomTemplates
                    where bomOption bom = (bomtmplName $ entityVal bom, optionId bom)

            h1_ "Add/edit 1C item"
            form_ [id_ "get", method_ "GET"] $ do
                input_
                    [ type_ "hidden"
                    , name_ "id"
                    , value_ (maybe "" (T.pack . show) mItemId)
                    ]
            form_ [id_ "save", method_ "POST"] $ do
                input_
                    [ type_ "hidden"
                    , name_ "redirectTo"
                    , value_ ("/items?type=" <> (maybe "" (T.pack . show. fromSqlKey) mItypeKey2show))
                    ]
                input_
                    [ type_ "hidden"
                    , name_ "id"
                    , value_ (maybe "" (T.pack . show) mItemId)
                    ]
            fieldset_ $ do
                div_ [ class_ "line" ] $ do
                    label_ "Type"
                    VL.selectHtml2
                        [ form_ "get", name_ "itemType", required_ ""]
                        itypesOptions
                        $ maybe "" (T.pack . show . fromSqlKey) mItypeKey2show 
                    div_ $ button_ [ form_ "get" ] "Send"
                div_ [ class_ "line" ] $ do
                    label_ "Item"
                    VL.selectHtml2
                        [ form_ "save", name_ "planItem", required_ ""]
                        pitemsOptions
                        $ maybe "" (T.pack . show) mPlanItemId
                div_ [ class_ "line"] $ do
                    label_ "Code 1C"
                    input_ [ form_ "save", type_ "number" , name_ "code1c" , required_ ""
                            , value_ (maybe "" (T.pack . show) $ showField itemCode1C)]
                div_ [ class_ "line" ] $ do
                    label_ "Part number"
                    input_ [ form_ "save", type_ "text" , name_ "pn"
                            , value_ (fromMaybe "" $ showField itemPn)]
                div_ [ class_ "line"] $ do
                    label_ "Description"
                    input_ [ form_ "save", type_ "text", name_ "description", required_ ""
                            , value_ (fromMaybe "" $ showField itemDescription)]
                div_ [ class_ "line"] $ do
                    label_ "1C UOM"
                    VL.selectHtml2
                        [ form_ "save", name_ "uom", required_ ""]
                        uoms2
                        $ fromMaybe "" $ showField itemUom 
                div_ [ class_ "line"] $ do
                    label_ "Conversion"
                    input_ [ form_ "save", type_ "number", name_ "conversion", step_ "any", required_ ""
                            , value_ (maybe "" (toFixed 3) $ showField itemConversion)]
                div_ [ class_ "line" ] $ do
                    label_ "Price"
                    input_ [ form_ "save", type_ "number", name_ "price", step_ "any", required_ ""
                            , value_ (maybe "" (toFixed 2) $ showField itemPrice)]
                div_ [ class_ "line" ] $ do
                    label_ "Obsolete?"
                    input_ $ [ form_ "save", type_ "checkbox", name_ "isObsolete", value_ "true"]  
                            <> (maybe [] (\o -> if o then [checked_] else []) $ showField itemIsObsolete)
                div_ [ class_ "line"] $ do
                    label_ "Bom template"
                    VL.selectHtml2
                        [ form_ "save", name_ "bomtmpl"]
                        bomsOptions 
                        $ maybe "" (T.pack . show . fromSqlKey) $ fromMaybe Nothing $ showField itemBomtmpl
                div_ [ class_ "buttons" ] $ do
                    button_ [ form_ "save" ] "Save"
                    button_ [ form_ "save", formaction_ "/deleteItem" ] "Delete"
                    button_ [ form_ "save", type_ "reset" ] "Reset"


itemsPropertiesSql :: T.Text
itemsPropertiesSql = [r|
    SELECT ??, ??, ??
    FROM item
    INNER JOIN plan_item
    ON item.plan_item = plan_item.id
    LEFT JOIN item_property
    ON item.id = item_property.item
    LEFT JOIN bomtmpl
    on bomtmpl.id = item.bomtmpl
    WHERE plan_item.item_type = ?
    ORDER BY item.description ASC, item.id ASC
|]

bomlinesSql :: T.Text
bomlinesSql = [r|
    SELECT ??, ??
    FROM bomlinetmpl
    INNER JOIN bomtmpl
    ON bomtmpl.id = bomlinetmpl.tmpl
    WHERE bomlinetmpl.childtype = ?
    ORDER BY bomtmpl.parenttype ASC, bomtmpl.id ASC
|]
