#!/bin/bash

TM_SESSION="itemmaster2"
TM_SOURCE="Main.hs"

tmux has-session -t $TM_SESSION
if [ $? != 0 ]; then
    tmux new-session -s $TM_SESSION -d
    tmux send-keys -t $TM_SESSION "vim $TM_SOURCE" C-m
    tmux split-window -t $TM_SESSION -v
    tmux set-window-option -t $TM_SESSION set-pane-width 70%
    tmux select-layout -t $TM_SESSION main-vertical
    tmux select-pane -t:.0
    tmux bind-key a send-keys -t:.1 "stack exec $TM_SESSION" C-m \; select-pane -t:.1
    tmux bind-key b send-keys -t:.1 "stack build" C-m \;
fi

tmux attach -t $TM_SESSION

