{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module Views where

import Prelude hiding (head, div, id)
import Data.Double.Conversion.Text (toFixed)
import GHC.Int
import Data.Monoid ((<>))
import Control.Applicative ((<|>))
import Data.Text (Text, pack, unpack, concat)
import Data.Either (either, Either(..))
import Data.Maybe (fromMaybe)
import Data.List (find, groupBy)
import Text.Blaze.Internal (MarkupM(Append))
import Text.Blaze.Html5
                (Html, docTypeHtml, head, body, title, select, (!), (!?), toHtml
                , toValue, form, h1, h2, div, legend, fieldset, input, a, td, tr
                , button, table, th, tbody, option, datalist, script, link, string
                , nav, footer, section, label, textarea)
import Text.Blaze.Html5.Attributes
                (name, value, selected, method, action, type_, href, formmethod
                , formaction, list, id, disabled, onclick, required, placeholder
                , rel, class_, step, enctype, rowspan, cols, rows)

import Database.Persist.Postgresql
import Text.RawString.QQ (r)

import Types
import qualified Csv


selectHtml :: String -> [(String, String)] -> String -> Html
selectHtml tagName options selItem =
    select ! name (toValue tagName) $ mapM_ optionTag (("","") : options) 
    where optionTag (descr, optId) = option
                    ! value (toValue optId)
                    !? (selItem == optId, selected "selected")
                    $ toHtml descr 
                    
editPlanItem :: Either Int64 (Entity PlanItem) -> [ Entity ItemType ]-> Html
editPlanItem initialValue itemTypes = do
            h1 "Add/Edit Item"
            form ! method "post" ! action "/editPlanItem"$ do
                either
                    (const $ input ! type_ "hidden")
                    (\a -> input ! type_ "hidden" ! name "id" ! value (toValue $ show $ fromSqlKey $ entityKey a))
                    initialValue
                fieldset $ do
                    div ! class_ "line" $ do
                        label "Type"
                        selectHtml "itemType" options (either show (show . fromSqlKey . planItemItemType . entityVal) initialValue)
                    div ! class_ "line" $ do
                        label "Name"
                        input ! type_ "text" ! name "name"
                            ! value (toValue $ either (const "") (planItemName . entityVal) initialValue)
                    div ! class_ "line" $ do
                        label "Price"
                        input ! type_ "number" ! name "price" ! step "any"
                            ! value (toValue $ either (const "0") ((toFixed 2) . planItemPrice . entityVal) initialValue)
                    div ! class_ "buttons" $ do
                        button "Save"
                        button ! type_ "reset" $ "Reset"
        where
            options = map (\entity -> (optionDescr entity, optionId entity)) itemTypes
            optionId entity = show $ fromSqlKey $ entityKey entity
            optionDescr entity =
                (getGroupName $ itemTypeGroup $ entityVal entity) ++ "-" ++ (Data.Text.unpack $ itemTypeName $ entityVal entity)


uploadsPage :: [(Entity StockUpload, Single Int64, Single Int64)] -> Bool -> Html
uploadsPage uploads isAuth = do
    h1 "Stock uploads"
    if isAuth
        then a ! href "/upload" $ "Upload data"
        else string ""
    form $ do
        table $ do
            tr $ do
                th "Date"
                th "On order records #"
                th "On hand records #"
                if isAuth
                    then th "Action"
                    else string ""
            tbody $ mapM_ uploadView uploads
    where
        uploadView (entity, ooCount, ohCount) = 
            tr $ do 
                td $ toHtml $ show $ stockUploadDate $ entityVal entity
                td $ toHtml $ show $ unSingle ooCount
                td $ toHtml $ show $ unSingle ohCount
                if isAuth
                    then
                        td $ button
                            ! type_ "submit"
                            ! formmethod "POST"
                            ! name "idToDelete"
                            ! value (toValue $ fromSqlKey $ entityKey entity) $
                            "D"
                    else string ""

uploadPage :: Html
uploadPage  = do
    h1 "Upload inventory balances"
    form ! method "POST" ! enctype "multipart/form-data" $ do
        fieldset$ do
            div ! class_ "line" $ do
                label "Date of balances"
                input ! type_ "date" ! name "date" ! required ""
            div ! class_ "line" $ do
                label "CSV file On hand"
                input ! type_ "file" ! name "csvOnhand" ! required ""
            div ! class_ "line" $ do
                label "CSV file On order"
                input ! type_ "file" ! name "csvOnorder" ! required ""
            div ! class_ "buttons" $ do
                button "Upload"
    div ""
    div "Fields for On Hand file: PlaceType, PlaceName, Ubication, Owner, Code1C, Status, Qty"
    div "Fields for On Order file: Rfq, Date, Code1C, Qty, Price, Eta"

uploadErrorsPage :: [Csv.OnOrder] -> [Csv.OnHand] -> Html
uploadErrorsPage ooErrors ohErrors = do
    h1 "Upload errors"
    h2 "Codes 1c form Open Orders not found:"
    table $ do
        tr $ do
            th "Code 1C"
            th "PN"
            th "Description"
            th "UOM"
            th "RFQ"
            th "RFQ date"
        tbody $ mapM_ ooLine ooErrors 
    h2 "Codes 1c from Stock on hand not found:"
    table $ do
        tr $ do
            th "Code 1C"
            th "PN"
            th "Description"
            th "UOM"
            th "Place"
            th "Ubication"
            th "Owner"
            th "Status"
        tbody $ mapM_ ohLine ohErrors 
    where
        ooLine e = tr $ do
            td $ toHtml $ Csv.code1cOnOrder e
            td $ toHtml $ Csv.pnOnOrder e
            td $ toHtml $ Csv.iDescriptionOnOrder e
            td $ toHtml $ Csv.uomOnOrder e
            td $ toHtml $ Csv.rfq e
            td $ toHtml $ show $ Csv.rfqDate e
        ohLine e = tr $ do
            td $ toHtml $ Csv.code1cOnHand e
            td $ toHtml $ Csv.pnOnHand e
            td $ toHtml $ Csv.iDescriptionOnHand e
            td $ toHtml $ Csv.uomOnHand e
            td $ toHtml $ Csv.placeName e
            td $ toHtml $ Csv.ubication e
            td $ toHtml $ Csv.owner e
            td $ toHtml $ Csv.getStatus $ Csv.status e


aditItemPage :: (Maybe Int64, Maybe Int64, Maybe Int64) -> Maybe Item 
                            -> ([ Entity ItemType ], [Entity PlanItem]) -> Html
aditItemPage (itypeId, pitemId, itemId) mItem (itemTypes, pitems) = do
    h1 "Add/edit 1C item"
    form $ do
        fieldset $ do
            input
                ! type_ "hidden"
                ! name "redirectTo"
                ! value (toValue ("/planItem?id=" <> (fromMaybe "" (fmap show pitemId))))
            input
                ! type_ "hidden"
                ! name "id"
                ! value (toValue (fromMaybe "" (fmap show itemId)))
            div ! class_ "line" $ do
                label "Type"
                selectHtml "itemType" itypesOptions $ fromMaybe "" (fmap show itypeId)
                div $ do
                    button ! formmethod "GET" ! formaction "/aditItem" $ "Send"
            div ! class_ "line" $ do
                label "Item"
                selectHtml "planItem" pitemsOptions $ fromMaybe "" (fmap show pitemId)
            div ! class_ "line" $ do
                label "Code 1C"
                input ! type_ "number" ! name "code1c" ! required ""
                        ! value (toValue $ fromMaybe "" (fmap (show . itemCode1C) mItem))
            div ! class_ "line" $ do
                label "Part number"
                input ! type_ "text" ! name "pn"
                        ! value (toValue $ fromMaybe "" (fmap itemPn mItem))
            div ! class_ "line" $ do
                label "Description"
                input ! type_ "text" ! name "description" ! required ""
                        ! value (toValue $ fromMaybe "" (fmap (itemDescription) mItem))
            div ! class_ "line" $ do
                label "1C UOM"
                selectHtml "uom" uoms $ Data.Text.unpack (fromMaybe "" (fmap (itemUom) mItem))

            div ! class_ "line" $ do
                label "Conversion"
                input ! type_ "number" ! name "conversion"  ! step "any" ! required ""
                        ! value (toValue $ fromMaybe "" (fmap ((toFixed 2) . itemConversion) mItem))
            div ! class_ "line" $ do
                label "Price"
                input ! type_ "number" ! name "price" ! step "any" ! required ""
                        ! value (toValue $ fromMaybe "" (fmap ((toFixed 2) . itemPrice) mItem))
            div ! class_ "buttons" $ do
                button ! formmethod "POST" ! formaction "/aditItem" $ "Save"
                button ! formmethod "POST" ! formaction "/deleteItem" $ "Delete"
                button ! type_ "reset" $ "Reset"
    where
            optionId entity = show $ fromSqlKey $ entityKey entity
            itypesOptions = map (\entity -> (itypeDescr entity, optionId entity)) itemTypes
            itypeDescr entity =
                (getGroupName $ itemTypeGroup $ entityVal entity)
                        ++ "-"
                        ++ (Data.Text.unpack $ itemTypeName $ entityVal entity)
            pitemsOptions = map (\pitem -> (unpack $ planItemName $ entityVal pitem, optionId pitem)) pitems


templatePage :: Html -> Html
templatePage html =
    docTypeHtml $ do
        head $ do
            title "Itemmaster"
            link ! rel "stylesheet" ! type_ "text/css" ! href "style.css"
        body $ do
            nav $ do
                div $ do
                    a ! href "/" $ "Home"
                    a ! href "/searchItems" $ "Search 1C Items"
                    a ! href "/checkCodes1C" $ "Check 1C Codes"
                    a ! href "/stockuploads" $ "Uploads"
                    a ! href "/properties" $ "Properties"
                    if not False then a ! href "/login" $ "LOGIN" else string ""
            section html 
            footer $ do
                div "Itemmaster"

homePage :: [(Single Text, Single Int64, Single Int64, Single Int64)] -> Html
homePage groupsStats = do
            h1 "Summary"
            table $ do
                tr $ do
                    th "Group"
                    th "Types qty"
                    th "Items qty"
                    th "1C items qty"
                tbody $ mapM_ itemGroupLine groups
        where
            itemGroupLine (descr, id) = 
                let (_, typesQty, pitemsQty, itemsQty) = fromMaybe (Single (pack id), Single 0, Single 0, Single 0) $ Data.List.find (\(elId, _, _, _) -> (unSingle elId) == pack id) groupsStats
                in
                    tr $ do 
                        td $ do
                            a ! href (toValue $ "group?id=" ++ id) $ toHtml $ getGroupName $ pack id
                        td $ toHtml $ unSingle typesQty
                        td $ toHtml $ unSingle pitemsQty
                        td $ toHtml $ unSingle itemsQty

groupPage :: Text -> [(Entity ItemType, Single Int64, Single Int64)] -> Bool -> Html
groupPage groupId itemTypes isAuth = do
    h1 $ toHtml $ "Group " ++ (getGroupName groupId) 
    if isAuth
        then div $ a ! href (toValue $ "aditType?group=" <> groupId) $ "Add subgroup"
        else string ""
    form $ do
        table $ do
            tr $ do
                th "id"
                th "Name"
                th "UOM"
                th "Recuperation"
                th "Items"
                th "1C items"
                if isAuth then th "Action" else string ""
            tbody $ mapM_ itemTypeLine itemTypes
    where
        itemTypeLine (itemType, piQty, iQty) =
            tr $ do 
                td (if isAuth
                    then a ! href (toValue $ "/aditType?id=" ++ key)$ toHtml key
                    else toHtml key)
                td $ do
                    a ! href (toValue $ "/planItems?type=" ++ key) $ (toHtml . itemTypeName . entityVal) itemType 
                td $ (toHtml . itemTypeUom . entityVal) itemType 
                td $ (toHtml . (toFixed 2) . itemTypeRecuperation . entityVal) itemType 
                td $ toHtml $ unSingle piQty
                td $ toHtml $ unSingle iQty
                if isAuth then td $ do
                    button
                        ! type_ "submit"
                        ! formmethod "POST"
                        ! formaction "deleteType"
                        ! name "id"
                        ! value (toValue key) $
                        "D"
                    else string ""
            where  key =show (fromSqlKey $ entityKey itemType)


planItemPage :: (Entity ItemType, Entity PlanItem) -> [(Entity Item, Maybe (Single Double), Maybe (Single Double), Maybe (Single Double))]
     -> [(Entity PlanItem, Maybe (Entity Item), Single Double, Maybe (Single Double), Maybe (Single Double))]
            -> Bool -> Html
planItemPage (itemType, planItem) items childrenWitems isAuth = do
    h1 $ toHtml $ "Item: " ++ (Data.Text.unpack $ planItemName $ entityVal planItem)  
    div $ do
        string  "Item type: "
        a ! href (toValue ("/planItems?type=" <> (show $ fromSqlKey $ entityKey itemType))) 
            $ toHtml (Data.Text.unpack $ itemTypeName $ entityVal itemType)
    div $ toHtml $ "UOM: " ++ (Data.Text.unpack $ itemTypeUom $ entityVal itemType)  
    div $ a ! href (toValue $ "/children?parent=" ++ (show $ fromSqlKey $ entityKey planItem)) $ "BOM"
    div $ a ! href (toValue $ "/parents?child=" ++ (show $ fromSqlKey $ entityKey planItem)) $ "Used in"
    h2 "Availability"
    if isAuth
        then a ! href (toValue $ ("aditItem?planItem=" :: String) ++ (show $ fromSqlKey $ entityKey planItem)) $ "Add item"
        else string ""
    form $ do
        input ! type_ "hidden" ! name "redirectTo" ! value (toValue ("/planItem?id=" ++ (show $ fromSqlKey$ entityKey planItem))) 
        table $ do
            tr $ do
                th "Code1C"
                th "PN"
                th "Description"
                th "UOM 1C"
                th "Conversion"
                th "On order"
                th "On hand READY"
                th "On hand Used"
                if isAuth then th "Price" else string ""
                if isAuth then th "Action" else string ""
            tbody $ mapM_ itemLine items
    h2 "Assembling options"
    table $ do
        tr $ do
            th "Item"
            th "Qty"
            th "Code 1C"
            th "PN"
            th "Description 1C"
            th "UOM 1C"
            th "Conversion"
            th "On Order"
            th "On Hand"
        --tbody $ mapM_ bomLine childrenWitems
        tbody $ mapM_ bomBlock $ groupedChildrenWitems childrenWitems

    where
        itemLine (item, mOnOrder, mOnHandReady, mOnHandUsed) =
            tr !? (itemIsMain $ entityVal item, class_ "selected") $ do 
                td (if isAuth
                    then do
                        a ! href (toValue ("/aditItem?id=" <> (toValue key)))
                            $ (toHtml . itemCode1C . entityVal) item 
                    else (toHtml . itemCode1C . entityVal) item)
                td $ (toHtml . itemPn . entityVal) item 
                td $ (toHtml . itemDescription . entityVal) item 
                td $ (toHtml . itemUom . entityVal) item 
                td $ (toHtml . (toFixed 2) . itemConversion . entityVal) item 
                td $ fromMaybe
                    (string "")
                    (fmap (\qty -> a ! href (toValue ("/stock?id=" ++ (show $ fromSqlKey $ entityKey item))) $ (toHtml . (toFixed 2) . unSingle) qty) mOnOrder)
                td $ fromMaybe
                    (string "")
                    (fmap (\qty -> a ! href (toValue ("/stock?id=" ++ (show $ fromSqlKey $ entityKey item))) $ (toHtml . (toFixed 2) . unSingle) qty) mOnHandReady)
                td $ fromMaybe
                    (string "")
                    (fmap (\qty -> a ! href (toValue ("/stock?id=" ++ (show $ fromSqlKey $ entityKey item))) $ (toHtml . (toFixed 2) . unSingle) qty) mOnHandUsed)
                if isAuth
                    then td $ (toHtml . (toFixed 2) . itemPrice . entityVal) item 
                    else string ""
                if isAuth
                    then td $ do
                        button 
                            ! type_ "submit"
                            ! formmethod "POST"
                            ! formaction "/setMainItem"
                            ! name "id"
                            ! value (toValue key) $
                            "*"
                        button 
                            ! type_ "submit"
                            ! formmethod "GET"
                            ! formaction "/aditItem"
                            ! name "id"
                            ! value (toValue key) $
                            "E"
                        button
                            ! type_ "submit"
                            ! formmethod "POST"
                            ! formaction "/deleteItem"
                            ! name "id"
                            ! value (toValue key) $
                            "D"
                    else string ""
            where key = show $ fromSqlKey $ entityKey item
        groupedChildrenWitems childrenWitems =
            groupBy groupFunc childrenWitems
            where
                groupFunc (planItem1, _, _, _, _) (planItem2, _, _, _, _) = planItem1 == planItem2
        bomBlock ((planItem, mItem, bomQty, mooQty, mohQty):otherItems) = Append
            (tr $ do
                td ! rowspan (toValue $ 1 + (length otherItems)) $ a ! href (toValue $ "/planItem?id=" ++ (show $ fromSqlKey $ entityKey planItem))
                        $ toHtml $ planItemName $ entityVal planItem
                td ! rowspan (toValue $ 1+ (length otherItems)) $ toHtml $ toFixed 2 $ unSingle bomQty
                td $ fromMaybe "" $ fmap (toHtml . itemCode1C . entityVal) mItem
                td $ toHtml $ fromMaybe "" $ fmap (itemPn . entityVal) mItem
                td !? (fromMaybe False $ fmap (itemIsMain . entityVal) mItem, class_ "selected")$ toHtml $ fromMaybe "" $ fmap (itemDescription . entityVal) mItem
                td $ fromMaybe "" $ fmap (toHtml . itemUom . entityVal) mItem
                td $ fromMaybe "" $ fmap (toHtml . (toFixed 2) . itemConversion . entityVal) mItem
                td $ fromMaybe
                    (string "")
                    (fmap (\qty -> a ! href (toValue ("/stock?id=" ++ (itemId mItem))) $ (toHtml . (toFixed 2) . unSingle) qty) mooQty)
                td $ fromMaybe
                    (string "")
                    (fmap (\qty -> a ! href (toValue ("/stock?id=" ++ (itemId mItem))) $ (toHtml . (toFixed 2) . unSingle) qty) mohQty))
            (mapM_ otherLine otherItems)
            where
                itemId mItem = fromMaybe "" $ fmap (show . fromSqlKey . entityKey) mItem
        otherLine (planItem, mItem, bomQty, mooQty, mohQty) =
            tr $ do
                td $ fromMaybe "" $ fmap (toHtml . itemCode1C . entityVal) mItem
                td $ toHtml $ fromMaybe "" $ fmap (itemPn . entityVal) mItem
                td !? (fromMaybe False $ fmap (itemIsMain . entityVal) mItem, class_ "selected") $ toHtml $ fromMaybe "" $ fmap (itemDescription . entityVal) mItem
                td $ fromMaybe "" $ fmap (toHtml . itemUom . entityVal) mItem
                td $ fromMaybe "" $ fmap (toHtml . (toFixed 2) . itemConversion . entityVal) mItem
                td $ fromMaybe
                    (string "")
                    (fmap (\qty -> a ! href (toValue ("/stock?id=" ++ (itemId mItem))) $ (toHtml . (toFixed 2) . unSingle) qty) mooQty)
                td $ fromMaybe
                    (string "")
                    (fmap (\qty -> a ! href (toValue ("/stock?id=" ++ (itemId mItem))) $ (toHtml . (toFixed 2) . unSingle) qty) mohQty)
            where
                itemId mItem = fromMaybe "" $ fmap (show . fromSqlKey . entityKey) mItem


childrenPage :: (Entity ItemType, Entity PlanItem) -> [ (Single Int64, Entity ItemType, Entity PlanItem, Single Double) ] -> Bool -> Html
childrenPage (itemType, planItem) childBoms isAuth = do
            h1 $ do
                string "BoM for: "
                a ! href (toValue $ "/planItem?id=" ++ (show $ fromSqlKey $ entityKey planItem))
                    $ toHtml $ planItemName $ entityVal planItem  
            div $ do
                string  "Item type: "
                a ! href (toValue ("/planItems?type=" <> (show $ fromSqlKey $ entityKey itemType))) 
                    $ toHtml (Data.Text.unpack $ itemTypeName $ entityVal itemType)
            div $ toHtml $ "UOM: " ++ (Data.Text.unpack $ itemTypeUom $ entityVal itemType)  
            if isAuth
                then a ! href (toValue $ ("aditBom?parent=" :: String)
                    ++ (show $ fromSqlKey $ entityKey planItem)) $ "Add child"
                else string ""
            if isAuth 
                then a ! href (toValue $ ("copyChildren?parent=" :: String)
                    ++ (show $ fromSqlKey $ entityKey planItem)) $ "Copy bom"
                else string ""
            form $ do
                let parentId = fromSqlKey $ entityKey planItem
                input ! type_ "hidden" ! name "parent" ! value (toValue parentId)
                input ! type_ "hidden" ! name "redirectTo" ! value (toValue ("/children?parent=" <> (pack $ show parentId)))
                table $ do
                    tr $ do
                        th "id"
                        th "Group"
                        th "Subgroup"
                        th "Item"
                        th "Uom"
                        th "Qty"
                        if isAuth then th "Action" else string ""
                    tbody $ mapM_ bomLine childBoms 
        where
            bomLine (bomId, childType, childItem, qty) =
                tr $ do 
                    td $ if isAuth
                        then a ! href (toValue $ "/aditBom?bomId=" ++ key) $ toHtml key
                        else toHtml key 
                    td $ (toHtml . getGroupName . itemTypeGroup . entityVal) childType 
                    td $ a 
                        ! href (toValue ("/planItems?type=" <> (show $ fromSqlKey $ entityKey childType))) 
                        $ (toHtml . itemTypeName . entityVal) childType 
                    td $ a
                        ! href (toValue $ "/planItem?id=" ++ (show $ fromSqlKey $ entityKey childItem))
                        $ (toHtml . planItemName . entityVal) childItem 
                    td $ toHtml $ itemTypeUom $ entityVal childType
                    td $ (toHtml . (toFixed 2) . unSingle) qty
                    if isAuth
                        then td $ do
                            button 
                                ! type_ "submit"
                                ! formmethod "GET"
                                ! formaction "/aditBom"
                                ! name "bomId"
                                ! value (toValue $ unSingle bomId) $
                                "E"
                            button
                                ! type_ "submit"
                                ! formmethod "POST"
                                ! formaction "/deleteBom"
                                ! name "child"
                                ! value (toValue key) $
                                "D"
                        else string ""
                where key = show $ fromSqlKey $ entityKey childItem

parentsPage :: (Entity ItemType, Entity PlanItem) -> [ (Entity ItemType, Entity PlanItem, Single Double) ] -> Bool ->Html
parentsPage (itemType, child) parentBoms isAuth = do
            h1 $ do
                string "Parents for: "
                a ! href (toValue $ "/planItem?id=" ++ (show $ fromSqlKey $ entityKey child))
                    $ toHtml $ planItemName $ entityVal child  
            div $ do
                string  "Item type: "
                a ! href (toValue ("/planItems?type=" <> (show $ fromSqlKey $ entityKey itemType))) 
                    $ toHtml (Data.Text.unpack $ itemTypeName $ entityVal itemType)
            div $ toHtml $ "UOM: " ++ (Data.Text.unpack $ itemTypeUom $ entityVal itemType)  
            if isAuth
                then a ! href (toValue $ ("add2parents?child=" :: String)
                    ++ (show $ fromSqlKey $ entityKey child)) $ "Add parents"
                else string ""
            form $ do
                let childId = fromSqlKey $ entityKey child
                input ! type_ "hidden" ! name "child" ! value (toValue childId)
                input ! type_ "hidden" ! name "redirectTo" ! value (toValue ("/parents?child=" <> (pack $ show childId)))
                table $ do
                    tr $ do
                        th "id"
                        th "Group"
                        th "Subgroup"
                        th "Item"
                        th "Qty"
                        if isAuth then th "Action" else string ""
                    tbody $ mapM_ bomLine parentBoms 
        where
            bomLine (parentType, parentItem, qty) =
                tr $ do 
                    td $ toHtml key 
                    td $ (toHtml . getGroupName . itemTypeGroup . entityVal) parentType 
                    td $ a
                        ! href (toValue ( "/planItems?type=" <> (show $ fromSqlKey $ entityKey parentType)))
                        $ (toHtml . itemTypeName . entityVal) parentType 
                    td $ a
                        ! href (toValue $ "/planItem?id=" ++ (show $ fromSqlKey $ entityKey parentItem))
                        $ (toHtml . planItemName . entityVal) parentItem 
                    td $ (toHtml . (toFixed 2) . unSingle) qty
                    if isAuth then 
                        td $ do
                            button
                                ! type_ "submit"
                                ! formmethod "POST"
                                ! formaction "/deleteBom"
                                ! name "parent"
                                ! value (toValue key) $
                                "D"
                        else string ""
                where key = show $ fromSqlKey $ entityKey parentItem


copyChildrenPage :: (Entity ItemType, Entity PlanItem)
                        -> Entity ItemType -> [ Entity ItemType ] -> [ Entity PlanItem ] -> Html
copyChildrenPage (parentType, parent) selectedType types candidates = do
            script $ toHtml clearValueJs

            h1 $ toHtml $ "Item: " ++ (unpack $ planItemName $ entityVal parent)  
            div $ toHtml $ "Type: " ++ (unpack $ itemTypeName $ entityVal parentType)
            div $ toHtml $ "UOM: " ++ (unpack $ itemTypeUom $ entityVal parentType)  
            form $ do
                fieldset $ do
                    input ! type_ "hidden" ! name "parent" ! value (toValue $ fromSqlKey $ entityKey parent)

                    div ! class_ "line" $ do
                        label "Type: "
                        input ! list "types" ! name "selectedType" ! value (toValue $ optionTypeValue selectedType)
                        datalist ! id "types" $ mapM_ (\el -> option $ toHtml $ optionTypeValue el) types
                        div $ do
                            button ! type_ "button" ! onclick "clearvalue(\"selectedType\")" $ "X"
                            button ! type_ "submit" ! formmethod "GET" ! formaction "/copyChildren" $ "Select" 

                    div ! class_ "line" $ do
                        let mChildToShow = Nothing
                        label "Item: "
                        input ! list "candidates" ! name "donor" ! value (toValue $ fromMaybe "" (fmap optionChildValue mChildToShow))
                        datalist ! id "candidates" $ mapM_ (\el -> option $ toHtml $ optionChildValue el) candidates
                        div $
                            button ! type_ "button" ! onclick "clearvalue(\"child\")" $ "X"

                    div ! class_ "buttons" $ do
                        button ! type_ "submit" ! formmethod "POST" ! formaction "/copyChildren" $ "Copy BOM" 
                        button ! type_ "reset" $ "Reset" 
    where
        optionTypeValue itype  =
                (pack $ show $ fromSqlKey $ entityKey itype)
                <> " "
                <> (pack $ getGroupName $ itemTypeGroup $ entityVal itype)
                <> "-"
                <> (itemTypeName $ entityVal itype)
        optionChildValue child  =
                (pack $ show $ fromSqlKey $ entityKey child)
                <> "-"
                <> (planItemName $ entityVal child)


aditChildPage :: (Entity ItemType, Entity PlanItem) -> Maybe (Entity ItemType, Entity PlanItem) -> Maybe (Entity Bom)
                        -> Maybe (Entity ItemType) -> [ Entity ItemType ] -> [ Entity PlanItem ] -> Html
aditChildPage (parentType, parent) mChildExtE mBom mSelectedType types candidates = do
            script $ toHtml clearValueJs

            let mTypeToShow = mSelectedType  <|> (fmap fst mChildExtE)
            h1 $ toHtml $ "Item: " ++ (unpack $ planItemName $ entityVal parent)  
            div $ toHtml $ "Type: " ++ (unpack $ itemTypeName $ entityVal parentType)
            div $ toHtml $ "UOM: " ++ (unpack $ itemTypeUom $ entityVal parentType)  
            form $ do
                fieldset $ do
                    input ! type_ "hidden" ! name "bomId" ! value (toValue $ fromMaybe "" (fmap (show . fromSqlKey . entityKey) mBom))
                    input ! type_ "hidden" ! name "parent" ! value (toValue $ fromSqlKey $ entityKey parent)

                    div ! class_ "line" $ do
                        label "Type: "
                        input ! list "types" ! name "selectedType" ! value (toValue $ fromMaybe "" (fmap optionTypeValue mTypeToShow))
                        datalist ! id "types" $ mapM_ (\el -> option $ toHtml $ optionTypeValue el) types
                        div $ do
                            button ! type_ "button" ! onclick "clearvalue(\"selectedType\")" $ "X"
                            button ! type_ "submit" ! formmethod "GET" ! formaction "/aditBom" $ "Select" 

                    div ! class_ "line" $ do
                        let mChildToShow = if (fmap fst mChildExtE) == mTypeToShow then fmap snd mChildExtE else Nothing
                        label "Item: "
                        input ! list "candidates" ! name "child" ! value (toValue $ fromMaybe "" (fmap optionChildValue mChildToShow))
                        datalist ! id "candidates" $ mapM_ (\el -> option $ toHtml $ optionChildValue el) candidates
                        div $
                            button ! type_ "button" ! onclick "clearvalue(\"child\")" $ "X"

                    div ! class_ "line" $ do
                        label "Quantity: "
                        input ! type_ "number" ! name "qty" ! step "any"
                            ! value (toValue $ fromMaybe "" (fmap ((toFixed 3) . bomQty . entityVal) mBom))
                    div ! class_ "buttons" $ do
                        button ! type_ "submit" ! formmethod "POST" ! formaction "/aditBom" $ "Save" 
                        button ! type_ "reset" $ "Reset" 
    where
        optionTypeValue itype  =
                (pack $ show $ fromSqlKey $ entityKey itype)
                <> " "
                <> (pack $ getGroupName $ itemTypeGroup $ entityVal itype)
                <> "-"
                <> (itemTypeName $ entityVal itype)
        optionChildValue child  =
                (pack $ show $ fromSqlKey $ entityKey child)
                <> "-"
                <> (planItemName $ entityVal child)

clearValueJs :: Text
clearValueJs= [r|
function clearvalue(tagname) {
  document.getElementsByName(tagname)[0].value = ""
}
|]

add2parentsPage :: (Entity ItemType, Entity PlanItem)
                        -> [ Entity ItemType ]
                        -> (Entity ItemType, String, [(Entity PlanItem, Single Double)])
                        -> Html
add2parentsPage (iType, item) types (filterType, filterName, candidates) = do
            script $ toHtml selectAllJs
            h1 $ toHtml $ "Item: " ++ (Data.Text.unpack $ planItemName $ entityVal item)  
            div $ toHtml $ "Type: " ++ (Data.Text.unpack $ itemTypeName $ entityVal iType)
            div $ toHtml $ "UOM: " ++ (Data.Text.unpack $ itemTypeUom $ entityVal iType)  
            form ! method "GET" ! action "/add2parents" $ do
                input ! type_ "hidden" ! name "child" ! value (toValue $ fromSqlKey $ entityKey item)
                let optionDescr itype = unpack $ (Data.Text.pack $
                        getGroupName (itemTypeGroup $ entityVal itype)) <> " - " <> (itemTypeName $ entityVal itype)
                let options = fmap (\itype -> (optionDescr itype, show $ fromSqlKey $ entityKey itype)) types
                selectHtml "type" options (show $ fromSqlKey $ entityKey filterType) 
                input ! name "filterName" ! value (toValue filterName) ! placeholder "Search by name"
                button ! type_ "submit" $ "Search" 
            form ! method "POST" ! action "/add2parents" $ do
                input ! type_ "hidden" ! name "child" ! value (toValue $ fromSqlKey $ entityKey item)
                div $ do
                    "Quantity: "
                    input ! type_ "number" ! name "qty" ! required "" ! step "any"
                table $ do
                    tr $ do
                        th "id"
                        th "Item"
                        th "Qty"
                        th $ input ! type_ "checkbox" ! onclick "toggle(this)" 
                    tbody ! id "candidates" $ mapM_ candidateLine candidates
                button ! type_ "submit" $ "Save"
                button ! type_ "reset" $ "Reset"
        where
            candidateLine (planItem, qty) =
                tr $ do
                    let key = fromSqlKey $ entityKey planItem
                    td $ toHtml key
                    td $ toHtml $ planItemName $ entityVal planItem
                    td $ (toHtml . (toFixed 2) . unSingle) qty 
                    td $ input ! type_ "checkbox" ! name (toValue key) !? (unSingle qty > 0, disabled "disabled")
selectAllJs :: Text
selectAllJs= [r|
function toggle(source) {
  checkboxes = document.getElementById('candidates').querySelectorAll("tr td input");
  for(var i=0, n=checkboxes.length;i<n;i++) {
    if (!checkboxes[i].disabled) { checkboxes[i].checked = source.checked; }
  }
}
|]

searchItemsPage :: Text -> [(Entity Item, Entity PlanItem, Entity ItemType) ] -> Bool -> Html
searchItemsPage pattern items isAuth = do
        h1 $ "Search 1C Items" 
        form ! method "GET" ! action "/searchItems" $ do
            input ! type_ "text" ! name "pattern" ! value (toValue pattern)
            button ! type_ "submit" $ "Search"
        form $ do
            input ! type_ "hidden" ! name "redirectTo" ! value (toValue ("searchItems?pattern=" <> pattern))
            table $ do
                tr $ do
                    th "Code1C"
                    th "PN"
                    th "Description"
                    th "1C UOM"
                    th "Main?"
                    th "Item"
                    th "UOM"
                    th "Conversion"
                    if isAuth then th "Price" else string ""
                    if isAuth then th "Action" else string ""
                tbody $ mapM_ itemLine items
    where
        itemLine (item, planItem, itemType) =
            tr $ do 
                td $ (toHtml . itemCode1C . entityVal) item 
                td $ (toHtml . itemPn . entityVal) item 
                td $ (toHtml . itemDescription . entityVal) item 
                td $ (toHtml . itemUom . entityVal) item 
                td $ (toHtml . show . itemIsMain . entityVal) item 
                td $ a ! href (toValue ("/planItem?id=" ++ (show $ fromSqlKey $ entityKey planItem)))
                        $ (toHtml . planItemName . entityVal) planItem 
                td $ (toHtml . itemTypeUom . entityVal) itemType 
                td $ (toHtml . (toFixed 2) . itemConversion . entityVal) item 
                if isAuth then td $ (toHtml . (toFixed 2) . itemPrice . entityVal) item else string ""
                if isAuth
                    then td $ do
                        button 
                            ! type_ "submit"
                            ! formmethod "GET"
                            ! formaction "/aditItem"
                            ! name "id"
                            ! value (toValue $ fromSqlKey $ entityKey item) $
                            "E"
                        button 
                            ! type_ "submit"
                            ! formmethod "POST"
                            ! formaction "/deleteItem"
                            ! name "id"
                            ! value (toValue $ fromSqlKey $ entityKey item) $
                            "D"
                    else string ""


planItemsPage :: Entity ItemType -> [(Entity PlanItem, Single Int64, Single Int64, Single Int64, Maybe (Single Int64), Maybe (Single Int64))] -> Bool -> Html
planItemsPage itemType items isAuth = do
            h1 $ do
                a ! href (toValue ("/group?id=" <> (itemTypeGroup $ entityVal itemType))) $ toHtml (getGroupName $ itemTypeGroup $ entityVal itemType)
                string " // "
                string $ (Data.Text.unpack . itemTypeName . entityVal) itemType
            div $ toHtml $ "UOM: " <> (itemTypeUom $ entityVal  itemType)
            div $ if isAuth
                then a ! href (toValue $ "editPlanItem?itype=" ++ (show $ fromSqlKey $ entityKey itemType))
                        $ "Add item"
                else string ""
            form $ do       
                table $ do
                    tr $ do
                        th "id"
                        th "Description"
                        th "Price"
                        th "1C Items#"
                        th "Ordered#"
                        th "OnHand# (N+U)"
                        th "Children#"
                        th "Parents#"
                        if isAuth then th "Action" else string ""
                    tbody $ mapM_ itemLine items
        where
            itemLine (item, itemsQty, parentsQty, childrenQty, mOnOrder, mOnHand) =
                tr $ do 
                    td $ if isAuth
                        then a ! href (toValue $ "/editPlanItem?id=" ++ key) $ toHtml key
                        else toHtml key 
                    td $ a ! href (toValue $ "/planItem?id=" ++ key) $ (toHtml . planItemName . entityVal) item 
                    td $ (toHtml . planItemPrice . entityVal) item 
                    td $ (toHtml $ unSingle itemsQty)
                    td $ toHtml $ fromMaybe "" $ fmap (show . unSingle) mOnOrder
                    td $ toHtml $ fromMaybe "" $ fmap (show . unSingle) mOnHand
                    td $ a ! href (toValue $ "/children?parent=" ++ key) $ (toHtml $ unSingle parentsQty)
                    td $ a ! href (toValue $ "/parents?child=" ++ key) $ (toHtml $ unSingle childrenQty)
                    if isAuth
                        then td $ do
                            button
                                ! type_ "submit"
                                ! formmethod "POST"
                                ! formaction "/deletePlanItem"
                                ! name "id"
                                ! value (toValue key) $
                                "D"
                        else string ""
                where  key = show $ fromSqlKey $ entityKey item

stockPage :: Entity Item -> [Entity OnOrder] -> [Entity OnHand] -> Html
stockPage item onOrders onHands = do
        h1 $ toHtml ("Item 1C " ++ (show $ itemCode1C $ entityVal item) ++ "-" ++ (unpack $ itemDescription $ entityVal item))
        h2 $ "On Order"
        table $ do
            tr $ do
                th "Rfq"
                th "Rfq date"
                th "Qty"
                th "Exw"
            tbody $ mapM_ onOrderLine onOrders
        h2 $ "On hand"
        table $ do
            tr $ do
                th "Place"
                th "Ubication"
                th "Owner"
                th "Status 1C"
                th "Qty"
                th "Status"
            tbody $ mapM_ onHandLine onHands
    where
        onOrderLine onOrder = do
            tr $ do
                td $ toHtml $ onOrderRfq $ entityVal onOrder
                td $ toHtml $ show $ onOrderDate $ entityVal onOrder
                td $ toHtml $ toFixed 2 $ onOrderQty $ entityVal onOrder
                td $ toHtml $ show $ onOrderEta $ entityVal onOrder
        onHandLine onHand = do
            tr $ do
                td $ toHtml $ onHandPlaceName $ entityVal onHand
                td $ toHtml $ onHandUbication $ entityVal onHand
                td $ toHtml $ onHandOwner $ entityVal onHand
                td $ toHtml $ onHandStatus1c $ entityVal onHand
                td $ toHtml $ toFixed 2 $ onHandQty $ entityVal onHand
                td $ toHtml $ onHandStatus $ entityVal onHand

checkItemsPage :: Html
checkItemsPage = do
    h1 "Check Items"
    form ! method "GET" ! action "/checkCodes1C" $ do
        textarea ! name "codes" ! cols "40" ! rows "5" ! placeholder "Space separated codes 1C" $ ""
        div ! class_ "buttons" $ do
            button ! type_ "submit" $ "Check"

checkItemsResultsPage :: [ Either Text (Entity Item, Entity PlanItem, Entity ItemType) ] -> Html
checkItemsResultsPage eItems = do
    h1 "Check Items Results"
    table $ do
        tr $ do
            th "Code1C"
            th "Description 1C"
            th "Item"
            th "Recuperation"
            th "Is Main?"
        tbody $ mapM_ tableLine eItems
    where
        tableLine eItem
            | Right (item, planItem, itemType) <- eItem  = do
                let isMain = itemIsMain $ entityVal item
                    recuperation = itemTypeRecuperation $ entityVal itemType 
                    error = (not isMain) || recuperation == 0
                tr !? (error, class_ "error") $ do
                    td $ toHtml $ itemCode1C $  entityVal item
                    td $ toHtml $ itemDescription $ entityVal item
                    td $ a ! href (toValue $ "/planItem?id=" ++ (show $ fromSqlKey $ entityKey planItem)) $ (toHtml . planItemName . entityVal) planItem 
                    td $ toHtml $ (toFixed 2) recuperation
                    td $ toHtml isMain
            | Left message <- eItem = do
                tr ! class_ "error" $ do
                    td $ toHtml message
                    td "NOT FOUND"
                    td ""
                    td ""
                    td ""


checkCode1cSql :: Text
checkCode1cSql = [r|
    SELECT ??, ??, ??
    FROM item
    INNER JOIN plan_item
    ON item.plan_item = plan_item.id
    INNER JOIN item_type
    ON plan_item.item_type = item_type.id
    WHERE code1_c = ?
|]

itemsQtySql :: Text 
itemsQtySql = [r|
    SELECT ??, oo.qty, oh.qtyReady, oh.qtyUsed
    FROM item
    LEFT JOIN (
        SELECT oo.item, sum(oo.qty) as qty
        FROM on_order oo
        INNER JOIN stock_upload u
        ON u.id = oo.upload
        WHERE u.date IN (SELECT max(date) FROM stock_upload)
        GROUP BY oo.item
    ) oo
    ON item.id = oo.item 
    LEFT JOIN (
        SELECT
            oh.item,
            sum(CASE oh.status WHEN 'Ready' THEN oh.qty ELSE 0 END) as qtyReady,
            sum(CASE oh.status WHEN 'Used' THEN oh.qty ELSE 0 END) as qtyUsed
        FROM on_hand oh
        INNER JOIN stock_upload u
        ON u.id = oh.upload
        WHERE u.date IN (SELECT max(date) FROM stock_upload)
        GROUP BY oh.item
    ) oh
    ON item.id = oh.item
    WHERE item.plan_item = ?
    ORDER BY item.description ASC, item.code1_c ASC
|]

itemsSql :: Text 
itemsSql = [r|
    SELECT ??  FROM item 
|]

itemTypesSql :: Text 
itemTypesSql = [r|
    SELECT ??  FROM item_type 
|]

parentsSql :: Text 
parentsSql= [r|
    SELECT ??, ??, bom.qty
    FROM plan_item
    INNER JOIN bom
    ON bom.parent = plan_item.id
    INNER JOIN item_type
    ON plan_item.item_type = item_type.id
    WHERE bom.child = ?
    ORDER BY plan_item.name ASC
|]

childrenSql :: Text 
childrenSql= [r|
    SELECT bom.id, ??, ??, bom.qty
    FROM plan_item
    INNER JOIN bom
    ON bom.child = plan_item.id
    INNER JOIN item_type
    ON plan_item.item_type = item_type.id
    WHERE bom.parent = ?
    ORDER BY plan_item.name ASC
|]
childrenSql2 :: Text 
childrenSql2= [r|
    SELECT ??, ??, bom.qty, oo.qty, oh.qty
    FROM plan_item
    INNER JOIN bom
    ON bom.child = plan_item.id
    INNER JOIN item_type
    ON plan_item.item_type = item_type.id
    LEFT JOIN item
    ON item.plan_item = plan_item.id
    LEFT JOIN (
        SELECT oo.item, sum(qty) as qty
        FROM on_order oo
        INNER JOIN stock_upload u
        ON oo.upload = u.id
        WHERE u.date IN (SELECT max(date) from stock_upload)
        GROUP BY oo.item
        ) oo
    ON oo.item = item.id
    LEFT JOIN (
            SELECT oh.item, sum(qty) as qty
            FROM on_hand oh
            INNER JOIN stock_upload u
        ON oh.upload = u.id
        WHERE
            u.date IN (SELECT max(date) from stock_upload)
        AND
            oh.status = 'Ready'
        GROUP BY oh.item
    ) oh 
    ON oh.item = item.id
    WHERE bom.parent = ?
    ORDER BY plan_item.name ASC
|]

planItemsWithTypesSql :: Text 
planItemsWithTypesSql = [r|
    SELECT ??, ?? 
    FROM plan_item
    INNER JOIN item_type
    ON plan_item.item_type = item_type.id
    ORDER BY item_type.group ASC, item_type.name ASC, plan_item.name ASC
|]

checkCyclesSql :: Text
checkCyclesSql = [r|
    WITH RECURSIVE children AS (
        SELECT ? ::bigint as id
        UNION 
        SELECT b.child
        FROM bom b
        INNER JOIN children c
        ON b.parent = c.id
    ) SELECT id
    FROM children
    WHERE children.id = ?
|]

planItemsStatsSql :: Text
planItemsStatsSql = [r|
    SELECT DISTINCT ??, coalesce(items.qty, 0), coalesce(children.qty, 0), coalesce(parents.qty, 0), items.ooQty, items.ohQty
    FROM plan_item
    LEFT JOIN (
        SELECT i.plan_item as id, count(i.id) as qty, sum(oo.qty * i.conversion) as ooQty, sum(oh.qty * i.conversion) as ohQty
        FROM item i
        LEFT JOIN (
            SELECT oo.item, sum(oo.qty) as qty
            FROM on_order oo
            INNER JOIN stock_upload u
            ON oo.upload = u.id
            WHERE u.date in (SELECT max(date) from stock_upload)
            GROUP BY oo.item
        ) oo
        ON i.id = oo.item
        LEFT JOIN (
            SELECT oh.item, sum(oh.qty) as qty
            FROM on_hand oh
            INNER JOIN stock_upload u
            ON oh.upload = u.id
            WHERE u.date in (SELECT max(date) from stock_upload)
                AND oh.status <> 'Omit'
            GROUP BY oh.item
        ) oh
        ON i.id = oh.item
        GROUP BY i.plan_item
    ) items
    ON items.id = plan_item.id
    LEFT JOIN (
        SELECT b.parent as id, count(b.child) as qty
        FROM bom b
        GROUP BY b.parent
        ) children
    ON children.id = plan_item.id
    LEFT JOIN ( 
        SELECT b.child as id, count(b.parent) as qty
        FROM bom b
        GROUP BY b.child
        ) parents
    ON parents.id = plan_item.id
    WHERE plan_item.item_type = ? 
    ORDER BY plan_item.name ASC

|]

itemTypesStatsSql :: Text
itemTypesStatsSql = [r|
    SELECT ??, piqty, cast(iqty as Integer)
    FROM item_type
    INNER JOIN (
        SELECT item_type.id, count(pitems.*) as piqty, coalesce(sum(pitems.qty),0) as iqty
        FROM item_type
        LEFT JOIN (
            SELECT pi.item_type, pi.id, count(i.*) as qty
            FROM plan_item pi
            LEFT JOIN item i
            ON i.plan_item = pi.id
            GROUP BY pi.item_type, pi.id
            ) pitems
        ON pitems.item_type = item_type.id
        WHERE item_type.group = ?
        GROUP BY item_type.id
    ) calc
    ON calc.id = item_type.id
    ORDER BY item_type.name ASC
|]

groupsStatsSql :: Text
groupsStatsSql= [r|
    SELECT it.group, count(distinct it.id), count(distinct pi.id), count(i.*)
    FROM item_type it
    LEFT JOIN plan_item pi
    ON it.id = pi.item_type
    LEFT JOIN item i
    ON pi.id = i.plan_item
    GROUP BY it.group
|]

searchItemsSql :: Text
searchItemsSql = [r|
    SELECT ??, ??, ??
    FROM item
    INNER JOIN plan_item
    ON item.plan_item = plan_item.id
    INNER JOIN item_type
    ON plan_item.item_type = item_type.id
    WHERE (item.code1_c || item.pn || lower(item.description)) LIKE ? 
    ORDER BY item.description ASC
    LIMIT 200
|]

candidates2ParentsSql :: Text
candidates2ParentsSql= [r|
    SELECT ??, coalesce(bf.qty,0) 
    FROM plan_item
    LEFT JOIN (
        SELECT b.parent, b.qty
        FROM bom b
        WHERE b.child = ?
    ) bf
    ON bf.parent = plan_item.id
    WHERE plan_item.item_type = ?
        AND lower(plan_item.name) LIKE ?
    ORDER BY plan_item.name ASC
|]

uploadsSql :: Text
uploadsSql= [r|
    SELECT ??, coalesce(oo.c,0), coalesce(oh.c,0)
    FROM stock_upload
    LEFT JOIN (
        SELECT u.id, count(oo.id) as c
        FROM stock_upload u
        LEFT JOIN on_order oo
        ON oo.upload = u.id
        GROUP BY u.id
    ) oo
    ON oo.id = stock_upload.id
    LEFT JOIN (
        SELECT u.id, count(oh.id) as c
        FROM stock_upload u
        LEFT JOIN on_hand oh
        ON oh.upload = u.id
        GROUP BY u.id
    ) oh
    ON oh.id = stock_upload.id
    ORDER BY stock_upload.date ASC
|]

stockOnHandSql :: Text
stockOnHandSql = [r|
    SELECT ??
    FROM on_hand
    INNER JOIN stock_upload u
    ON u.id = on_hand.upload
    WHERE u.date IN (SELECT max(date) FROM stock_upload)
            AND on_hand.item = ?
|]

stockOnOrderSql :: Text
stockOnOrderSql = [r|
    SELECT ??
    FROM on_order
    INNER JOIN stock_upload u
    ON u.id = on_order.upload
    WHERE u.date IN (SELECT max(date) FROM stock_upload)
            AND on_order.item = ?
|]
