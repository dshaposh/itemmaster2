{-# LANGUAGE OverloadedStrings #-}
--{-# LANGUAGE FlexibleContexts #-}
--{-# LANGUAGE QuasiQuotes #-}
--{-# LANGUAGE ScopedTypeVariables #-}

module Parser where

--import GHC.Int
--import Data.Monoid ((<>))
--import Control.Monad.IO.Class (liftIO)
--import Data.List (groupBy, group, head, sort, find)
import Data.Maybe (fromMaybe)
import qualified Data.Text as T
--import Data.Double.Conversion.Text (toFixed, toShortest)
--import Data.Monoid (mempty)
--
import Control.Applicative((<|>))
--import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
--import Control.Monad.Trans (lift)

import Text.ParserCombinators.Parsec (eof, try, parse, letter, digit, char, spaces, sepBy, many, Parser, many1, oneOf)


data PropCalc = Copy
              | Token String
              | Map [(String, String)]

parseCopy :: Parser PropCalc
parseCopy = try $ char '=' >> eof >> return Copy

parseToken :: Parser PropCalc
parseToken = do
    str <- many $ letter <|> digit <|> (oneOf ".\"")
    eof
    return $ Token str 

parseMap :: Parser PropCalc
parseMap = try $ fmap Map $ sepBy parsePair spaces
    where
        parsePair = do
            x1 <- many $ letter <|> digit <|> (oneOf ".\"")
            char '-'
            x2 <- many $ letter <|> digit <|> (oneOf ".\"")
            return (x1, x2)

parsePropCalc :: Parser PropCalc
parsePropCalc = parseCopy <|> parseMap <|> parseToken

evalProp :: T.Text -> PropCalc -> T.Text
evalProp input Copy = input
evalProp input (Token str) = T.pack str
evalProp input (Map map) = T.pack $ fromMaybe "Not exhaustive map" $ lookup (T.unpack input) map

parseAndEvalProp :: T.Text -> T.Text -> T.Text
parseAndEvalProp txt2parse input = evalProp input parsed
    where
        parsed = case parse parsePropCalc "imparser" (T.unpack txt2parse) of
            Left err -> Token "Parse error" 
            Right val -> val


data QtyCalc = Identity 
             | Number Int
             | List [(Char, Int)]


parseIdentity :: Parser QtyCalc 
parseIdentity = char '=' >> eof >> return Identity 

parseNumber :: Parser QtyCalc 
parseNumber = fmap (Number . read) $ many1 $ digit <|> char '.'

parseList :: Parser QtyCalc
parseList = fmap List $ sepBy parsePair spaces
    where
        parsePair = do
            op <- oneOf "+-*"
            number <- fmap read $ many1 digit
            return (op, number)

parseExpr :: Parser QtyCalc
parseExpr = parseIdentity <|> parseNumber <|> parseList

evalExpr :: Maybe Int -> QtyCalc -> Int
evalExpr (Just i) Identity = i
evalExpr Nothing Identity = 0
evalExpr _ (Number i) = i
evalExpr Nothing (List _) = 0
evalExpr (Just i) (List ops) = foldl f i ops
    where
        f z ('+', x) = z + x
        f z ('-', x) = z - x
        f z ('*', x) = z * x

calculate :: T.Text -> Maybe Int -> Int
calculate input mi = evalExpr mi parsed
    where
        parsed = case parse parseExpr "imparser" (T.unpack input) of
            Left err -> Number 0
            Right val -> val

