{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}

module ForExcel.Items where

import GHC.Int
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import Database.Persist.Postgresql(Single, unSingle)
import Text.RawString.QQ (r)
import Data.Text (Text, pack)
import qualified Text.Blaze.Html5 as H hiding (main)
import qualified Text.XML.Generator as XML

type Item =
    ( Single String
    , Single String
    , Single String
    , Single Int64
    , Single String
    , Single String
    , Single Double
    , Single Double
    )

itemsXml :: [ Item ] -> BSL.ByteString
itemsXml items =
    BSL.pack $ BS.unpack $
        XML.xrender $
            XML.doc XML.defaultDocInfo $
                XML.xelem "items" $
                    XML.xelems $ map
                        (\(group, name, pmethod, code1c, description, uom, price, packageSize) ->
                            XML.xelem
                                "item"
                                (XML.xattr "code1c" (pack $ show (unSingle code1c))
                                XML.<>
                                XML.xattr "group" (pack $ unSingle group)
                                XML.<>
                                XML.xattr "name" (pack $ unSingle name)
                                XML.<>
                                XML.xattr "planningMethod" (pack $ unSingle pmethod)
                                XML.<>
                                XML.xattr "uom" (pack $ unSingle uom)
                                XML.<>
                                XML.xattr "price" (pack $ show (unSingle price))
                                XML.<>
                                XML.xattr "packageSize" (pack $ show (unSingle packageSize))
                                XML.<#>
                                XML.xtext (pack (unSingle description))))
                        items 

itemsSql :: Text 
itemsSql = [r|
    SELECT
        it.group,
        it.name,
        it.planning_method,
        i.code1_c as code1C,
        i.description,
        i.uom,
        i.price,
        i.package_size
    FROM
        item i
    INNER JOIN
        item_type it
    ON
        i.item_type=it.id
    ORDER BY
        it.group,
        it.name,
        i.description
|]

