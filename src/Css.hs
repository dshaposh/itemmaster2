{-# LANGUAGE OverloadedStrings #-}
module Css where

import Prelude hiding (div, not)
import Clay
import Data.Monoid


hColor :: Color
hColor = rgb 41 41 41 

buttonColor :: Color
buttonColor = rgb 102 179 255

borders :: Color
borders = rgb 140 140 140

lMarginVal = px 50

myCss = render $ do
    (input <> select) ? do
        boxSizing borderBox

    body ? do
        sym margin (px 0)
        sym padding (px 0)
        fontFamily [] [sansSerif]
        background  (rgb 252 252 252)

    nav ? do
        background hColor
        color (rgb 245 245 245)
    (nav |> div) ? do
        fontSize (px 20)
        marginLeft lMarginVal
        sym2 padding (px 20) 0 
        a ? do
            lineHeight (px 50)
            display inlineBlock
            paddingRight (px 5)
            textDecoration none
        ( a # link <> a # visited) ? do
            color (rgb 250 250 250)
        a # hover ? do
            background (rgb 255 255 0)  
            color (rgb 0 0 0)

    footer ? do
        marginTop (px 50)
    (footer |> div) ? do
        marginLeft lMarginVal

    section ? do
        marginLeft lMarginVal
--        width (pct 70)

    (table <> th <> td) ? do
        border solid (px 1) borders
        borderCollapse collapse
        fontSize (px 15)
        a ? do
            display block
            textDecoration none
            color (rgb 0 0 200)
        a # hover ? do
            textDecoration underline
            background (rgb 220 220 220)

    tr # ".selected" ? do
        background (rgb 220 220 220)
    tr # ".error" ? do
        background (rgb 255 155 155)
    td # ".selected" ? do
        background (rgb 220 220 220)
    th ? do
        background (rgb 77 77 77) 
        color (rgb 221 221 221) 
    (td <> th) ? do
        sym2 padding (px 2) (px 7) 
    (td |> input) ? do
        borderStyle none

    fieldset ? do
        maxWidth (px 550)
        fontSize (px 16)
        button ? do
            borderColor buttonColor
            background buttonColor
            borderStyle solid 

        div # ".buttons" ? do
            textAlign (alignSide sideCenter)
            button ? do
                sym2 padding (px 5) (px 20)
            button |+ button ? do
                marginLeft (px 30)

        div # ".line" ? do
            sym2 margin (px 4) (px 0)
            display inlineTable
            width (pct 100)
            (input <> select <> button) ? do
                paddingTop (px 5)
                paddingBottom (px 5)
                borderWidth (px 1)
            label ? do
                display tableCell
                width (pct 25)
            (select <> input)  ? do
                fontSize (px 16)
                display tableCell
                borderStyle solid 
                borderColor borders
                paddingRight (px 5)
                paddingLeft (px 5)
                width (pct 100)
            div ? do
                display tableCell
                width (pct 1)
                whiteSpace nowrap
                button # firstChild ? do
                    marginLeft (px 5)
