{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}

module ForExcel where

import Data.Maybe (fromMaybe)
import Database.Persist (Entity, entityVal, entityKey)
import Database.Persist.Sql (Single, unSingle, fromSqlKey)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import Text.RawString.QQ (r)
import Data.Text (Text, pack)
import Text.XML.Generator (doc, xrender, xelem, xelems, xattr, (<>), (<#>), xtext, defaultDocInfo)
import Data.Double.Conversion.Text (toFixed)

import qualified Types as T

itemsXml :: [ (Entity T.Item, Entity T.PlanItem, Entity T.ItemType) ] -> BSL.ByteString
itemsXml entityItems =
    BSL.pack $ BS.unpack $
        xrender $
            doc defaultDocInfo $
                xelem "items" $
                    xelems $ map
                        (\(entityItem, entityPlanItem, entityType) ->
                            let item = entityVal entityItem
                                planItem = entityVal entityPlanItem
                                itemType = entityVal entityType
                            in
                                xelem
                                    "item"(
                                    xattr "code1c" (pack $ show (T.itemCode1C item))
                                    <> xattr "pn" (T.itemPn item)
                                    <> xattr "name" (T.itemDescription item)
                                    <> xattr "uom" (T.itemUom item)
                                    <> xattr "conversion" (toFixed 3 $ T.itemConversion item)
                                    <> xattr "price" (toFixed 3 $ T.itemPrice item)
                                    <> xattr "isMain" (pack $ show $ T.itemIsMain item)
                                    <> xattr "planItemId" (pack $ show $ fromSqlKey $ T.itemPlanItem item)
                                    <> xattr "planItemName" (T.planItemName planItem)
                                    <> xattr "itemType" (T.itemTypeName itemType)
                                    <> xattr "group" (pack $ T.getGroupName $ T.itemTypeGroup itemType)))
                        entityItems 

bomsXml :: [(Entity T.Bom, Single Text, Single Text)] -> BSL.ByteString
bomsXml boms =
    BSL.pack $ BS.unpack $
        xrender $
            doc defaultDocInfo $
                xelem "boms" $
                    xelems $ map
                        (\(bom, parentName, childName) ->
                            xelem
                                "bom" (
                                xattr "parentId" (pack $ show $ fromSqlKey $ T.bomParent $ entityVal bom)
                                <> xattr "parentName" (unSingle parentName)
                                <> (xattr "childId" (pack $ show $ fromSqlKey $ T.bomChild $ entityVal bom))
                                <> xattr "childName" (unSingle childName)
                                <> xattr "qty" (toFixed 3 $ T.bomQty $ entityVal bom)))
                        boms 
boms2Xml :: [(Entity T.Item, [T.BomFromTmpl])] -> BSL.ByteString
boms2Xml boms =
    let showItem itemEnt =
            xelem
                "child"
                (xattr "childId" (pack $ show $ fromSqlKey $ entityKey itemEnt)
                    <> xattr "childCode1C" (pack $ show $ T.itemCode1C $ entityVal itemEnt)
                    <> xattr "childName" (T.itemDescription $ entityVal itemEnt)
                    <> xattr "uom" (T.itemUom $ entityVal itemEnt)
                    <> xattr "conversion" (toFixed 3 $ T.itemConversion $ entityVal itemEnt)
                )
        showline bomline =
            xelem
                "bomline"
                (xattr "bomtmpllineId" (pack $ show $ fromSqlKey $ entityKey $ T.bomlineTmpl bomline)
                    <> xattr "posOnDrawing" (T.bomlinetmplPosOnDrawing $ entityVal $ T.bomlineTmpl bomline)
                    <> xattr "childTypeId" (pack $ show $ fromSqlKey $ T.bomlinetmplChildtype $ entityVal $ T.bomlineTmpl bomline)
                    <> xattr "childTypeName" (T.itemTypeName $ entityVal $ T.lineType bomline)
                    <> xattr "childTypeUom" (T.itemTypeUom $ entityVal $ T.lineType bomline)
                    <> xattr "qty" (pack $ show $ T.evalQty bomline)
                    <> xattr "comment" (fromMaybe "" $ T.bomlinetmplComment $ entityVal $ T.bomlineTmpl bomline)
                    <#> (xelems $ map showItem $ T.items bomline)
                )
        showbom (itemEnt, lines) =
            xelem
                "bom"
                (xattr "parentId" (pack $ show $ fromSqlKey $ entityKey itemEnt)
                    <> xattr "parentName" (T.itemDescription $ entityVal itemEnt)
                    <> xattr "parentCode1C" (pack $ show $ T.itemCode1C $ entityVal itemEnt)
                    <> xattr "bomtmplId" (maybe "" (pack . show . fromSqlKey) $ T.itemBomtmpl $ entityVal $ itemEnt)
                    <#> (xelems $ map showline lines) 
                )
    in
        BSL.pack $ BS.unpack $
            xrender $
                doc defaultDocInfo $
                    xelem "boms" $ xelems $ map showbom boms



planItemsXml :: [(Entity T.PlanItem, Entity T.ItemType, Maybe (Entity T.Item))] -> BSL.ByteString
planItemsXml planItemsExt =
    BSL.pack $ BS.unpack $
        xrender $
            doc defaultDocInfo $
                xelem "planitems" $
                    xelems $ map
                        (\(planItemEntity, itemTypeEntity, mItemEntity) ->
                            let planItem = entityVal planItemEntity
                                itemType = entityVal itemTypeEntity
                                itemProp func mb = (fromMaybe "" (fmap func mb))
                            in xelem
                                "planItem" (
                                xattr "id" (pack $ show $ fromSqlKey $ entityKey planItemEntity)
                                <> xattr "name" (T.planItemName planItem)
                                <> xattr "group" (pack $ T.getGroupName $ T.itemTypeGroup itemType)
                                <> xattr "typeId" (pack $ show $ fromSqlKey $ entityKey itemTypeEntity)
                                <> xattr "typeName" (T.itemTypeName itemType)
                                <> xattr "recuperation" (toFixed 3 $ T.itemTypeRecuperation itemType)
                                <> xattr "uom" (T.itemTypeUom itemType)
                                <> xattr "itemCode1C" (itemProp (pack . show . T.itemCode1C . entityVal) mItemEntity)
                                <> xattr "itemPN" (itemProp (T.itemPn . entityVal) mItemEntity)
                                <> xattr "itemDescription" (itemProp (T.itemDescription . entityVal) mItemEntity)
                                <> xattr "itemUOM" (itemProp (T.itemUom . entityVal) mItemEntity)
                                <> xattr "itemPrice" (itemProp ((toFixed 3) . T.itemPrice . entityVal) mItemEntity)
                                <> xattr "itemConversion" (itemProp ((toFixed 3) . T.itemConversion . entityVal) mItemEntity)))
                                -- <#> xtext (T.planItemName planItem)))
                        planItemsExt 

stockXml :: [(Entity T.Item, Entity T.OnOrder)] -> [(Entity T.Item, Entity T.OnHand)] -> BSL.ByteString
stockXml onOrders onHands =
    BSL.pack $ BS.unpack $
        xrender $
            doc defaultDocInfo $
                xelem "stock" $
                    xelems $ (map
                        (\(itemEntity, onOrderEntity) ->
                            let item = entityVal itemEntity
                                onOrder = entityVal onOrderEntity
                            in xelem
                                "stockRecord" (
                                xattr "recId" (pack $ show $ fromSqlKey $ entityKey itemEntity)
                                <> xattr "planItemId" (pack $ show $ fromSqlKey $ T.itemPlanItem item)
                                <> xattr "Code1C" (pack $ show $ T.itemCode1C item)
                                <> xattr "PN" (T.itemPn item)
                                <> xattr "Description" (T.itemDescription item)
                                <> xattr "Uom" (T.itemUom item)
                                <> xattr "Conversion" ( (toFixed 3) $ T.itemConversion item)
                                <> xattr "Status" "Ordered"
                                <> xattr "Qty" (toFixed 3 $ T.onOrderQty onOrder)

                                <> xattr "Rfq" (T.onOrderRfq onOrder)
                                <> xattr "Date" (pack $ show $ T.onOrderDate onOrder)
                                <> xattr "Eta" (pack $ show $ T.onOrderEta onOrder)
                                <> xattr "Price" (toFixed 3 $ T.onOrderPrice onOrder)))
                        onOrders) ++
                    (map
                        (\(itemEntity, onHandEntity) ->
                            let item = entityVal itemEntity
                                onHand = entityVal onHandEntity
                            in xelem
                                "stockRecord" (
                                xattr "recId" (pack $ show $ fromSqlKey $ entityKey itemEntity)
                                <> xattr "planItemId" (pack $ show $ fromSqlKey $ T.itemPlanItem item)
                                <> xattr "Code1C" (pack $ show $ T.itemCode1C item)
                                <> xattr "PN" (T.itemPn item)
                                <> xattr "Description" (T.itemDescription item)
                                <> xattr "Uom" (T.itemUom item)
                                <> xattr "Conversion" ( (toFixed 3) $ T.itemConversion item)
                                <> xattr "Status" (T.onHandStatus onHand)
                                <> xattr "Qty" (toFixed 3 $ T.onHandQty onHand)

                                <> xattr "PlaceName" (T.onHandPlaceName onHand)
                                <> xattr "Status1C" (T.onHandStatus1c onHand)
                                <> xattr "Ubication" (T.onHandUbication onHand)
                                <> xattr "Owner" (T.onHandOwner onHand)))
                        onHands)

itemsSql :: Text 
itemsSql = [r|
    SELECT ??, ??, ??
    FROM item
    INNER JOIN plan_item
    ON item.plan_item = plan_item.id
    INNER JOIN item_type
    ON plan_item.item_type = item_type.id
    ORDER BY plan_item.item_type, item.description
|]

bomsSql :: Text 
bomsSql = [r|
    SELECT ??, parents.name, children.name
    FROM bom 
    INNER JOIN plan_item parents 
    ON bom.parent = parents.id
    INNER JOIN plan_item children
    ON bom.child = children.id
|]

planItemsSql :: Text 
planItemsSql = [r|
    SELECT ??, ??, ??
    FROM plan_item 
    INNER JOIN item_type
    ON plan_item.item_type = item_type.id
    LEFT JOIN (
        SELECT *
        FROM item
        WHERE item.is_main is True
        ) item
    ON plan_item.id = item.plan_item
    ORDER BY item_type.group, item_type.id
|]

onOrderSql :: Text 
onOrderSql = [r|
    SELECT ??, ??
    FROM on_order
    INNER JOIN item
    ON on_order.item = item.id
    INNER JOIN stock_upload u
    ON on_order.upload = u.id 
    WHERE u.date = ?
    ORDER BY item.plan_item ASC, item.description ASC
|]

onHandSql :: Text 
onHandSql = [r|
    SELECT ??, ??
    FROM on_hand
    INNER JOIN item
    ON on_hand.item = item.id
    INNER JOIN stock_upload u
    ON on_hand.upload = u.id 
    WHERE u.date = ? AND on_hand.status <> 'Omit'
    ORDER BY item.plan_item ASC, item.description ASC
|]
