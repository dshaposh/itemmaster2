{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}

module Main where


import Prelude hiding(readFile)
import qualified System.Directory
import GHC.Int (Int64)
import Data.Monoid ((<>))
import Data.List (groupBy)
import Data.Text as T (Text, pack, unpack, concat, replace, toLower, breakOn, length, strip)
import Data.Text.IO (readFile)
import Data.Text.Encoding (encodeUtf8)
import Data.Text.Lazy.Read (decimal, double)
import Text.Read (readMaybe)
import Data.Either (either, Either(..))
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.IO as TL
import qualified Web.Scotty as S
import qualified Text.Blaze.Html.Renderer.Pretty as R
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.Static hiding ((<|>))

import Database.Persist
import Database.Persist.Postgresql
import Database.Persist.Sql as SQL (SqlBackend, insert, replace)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Trans.Reader (ReaderT) 
import Data.Pool (Pool)
import Control.Monad.Logger

import PropertyType (PropertyType(..), allPropertyTypes, RepworkRule(..))
import Types
import Views
import qualified ViewItems
import qualified ViewBom
import qualified ViewsLucid as VL
import qualified Lucid.Base as Lucid
import ForExcel
import Css
import qualified Csv as Csv

import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe
import Control.Applicative ((<|>))
import Control.Monad (unless)
import Data.Maybe (fromMaybe, catMaybes, mapMaybe, isJust)

import Network.Wai.Middleware.HttpAuth (basicAuth, authIsProtected, AuthSettings, extractBasicAuth)
import Network.Wai (rawPathInfo, requestHeaders)
import Network.HTTP.Types.Status (unauthorized401)
import Network.HTTP.Types.Header (RequestHeaders(..), hAuthorization)
import Web.Scotty.Internal.Types (getReq, ActionT(..), ScottyError(..))
import Control.Monad.Reader (ask, liftM)
import Data.ByteString (ByteString(..))
import qualified Data.ByteString.Lazy as BL (ByteString(..))
import qualified Data.Csv as Csv
import qualified Data.Vector as V

import Data.Time.Calendar (Day(..))
import Data.Time.Format (parseTimeM, defaultTimeLocale)


import Network.Wai.Parse (fileContent, FileInfo(..))


import Control.Monad.Except (ExceptT(..), runExceptT)

doMigrations :: ReaderT SqlBackend IO ()
doMigrations = runMigration migrateAll

runDb pool query = liftIO (runSqlPool query pool)

runDbGet pool key =
    runSqlPool (get key) pool >>= (\a -> case a of
        Just a -> liftIO $ return a
        Nothing -> S.raise "Returned Nothing")

toSearchParam :: Text -> PersistValue
toSearchParam str = toPersistValue ("%" <> (T.replace "*" "%" (toLower str)) <> "%")

configFile :: String
configFile = "itemmaster.conf"

homeDir :: String
homeDir = "/opt/itemmaster2/"

getConfFile = do
    isConfExist <- System.Directory.doesFileExist (homeDir ++ configFile)
    return $ if isConfExist then homeDir ++ configFile
                else configFile


getQueryParam :: TL.Text -> S.ActionM (Maybe Int64)
getQueryParam name = do
    strNum :: TL.Text <- (S.param name `S.rescue` (const $ return ""))
    case (decimal strNum) of
        Right (num, _) -> return $ Just num
        Left _ -> return Nothing

getQueryParamMT :: TL.Text -> MaybeT S.ActionM Int64
getQueryParamMT name = MaybeT $ do
    strNum :: TL.Text <- (S.param name `S.rescue` (const $ return ""))
    case (decimal strNum) of
        Right (num, _) -> return $ Just num
        Left _ -> return Nothing

checkCredentials :: (Monad m, ScottyError e) => ActionT e m Bool
checkCredentials = do 
    rh :: RequestHeaders <- liftM requestHeaders (ActionT $ liftM getReq ask)
    return $ fromMaybe False $ fmap (\(u, p) -> p == "dima" && p == "dima") $ (lookup hAuthorization rh) >>= extractBasicAuth 

runDbGetMT pool key = MaybeT $ runDb pool $ get key

main :: IO ()
main = do
    TL.writeFile "style.css" myCss

    confFile <- getConfFile
    connStr <- readFile confFile
    pool <- runStdoutLoggingT $ createPostgresqlPool (encodeUtf8 connStr) 10

    runDb pool doMigrations

    S.scotty 3001 $ do
        --S.middleware $ staticPolicy (noDots >-> addBase "static")
        S.middleware logStdoutDev

        S.middleware $ basicAuth
            (\u p -> return $ u == "dima" && p == "dima")
            ("Itemmaster" { authIsProtected = (\req -> return $ rawPathInfo req == "/login") } :: AuthSettings)

        S.get "/login" $ do
            S.html "<h1>Welcome!!!</h1>"

        S.get "/style.css" $ S.file "style.css"

        S.get "/" $ do
            groupsStats :: [(Single Text, Single Int64, Single Int64, Single Int64)] <- runDb pool $ rawSql groupsStatsSql []
            S.html $ TL.pack $ R.renderHtml $ templatePage $ homePage groupsStats

        S.get "/searchItems" $ do
            isAuth <- checkCredentials
            pattern :: Text <- (S.param "pattern" `S.rescue` (const $ return ""))
            list :: [(Entity Item, Entity PlanItem, Entity ItemType)] <- runDb pool (rawSql searchItemsSql [toSearchParam pattern])
            S.html $ TL.pack $ R.renderHtml $ templatePage $ searchItemsPage pattern list isAuth

        S.get "/group" $ do
            isAuth <- checkCredentials
            groupId :: Text <- S.param "id" 
            types :: [(Entity ItemType, Single Int64, Single Int64)] <- runDb pool $ rawSql itemTypesStatsSql [toPersistValue groupId]
            S.html $ Lucid.renderText $ VL.templatePage $ VL.groupPage groupId types isAuth

        S.get "/bomTemplates" $ do
            isAuth <- checkCredentials
            typeId :: Int64 <- S.param "type" 
            let key :: Key ItemType = toSqlKey typeId
            itemType :: ItemType <- runDbGet pool key
            html <- lift $ Lucid.renderTextT $ ViewBom.bomTemplates (Entity key itemType) pool isAuth
            S.html html

        S.get "/bomTemplate" $ do
            isAuth <- checkCredentials
            typeId :: Int64 <- S.param "id" 
            let key :: Key Bomtmpl = toSqlKey typeId
            tmpl :: Bomtmpl <- runDbGet pool key
            html <- lift $ Lucid.renderTextT $ ViewBom.template (Entity key tmpl) pool isAuth
            S.html html

        S.get "/aditBomTemplate" $ do
            isAuth <- checkCredentials
            tmplId :: Int64 <- S.param "id" 
            lineId :: Maybe Int64 <- getQueryParam "line" 
            mChildGroup :: Maybe Int64 <- getQueryParam "childGroup" 
            let tmplKey :: Key Bomtmpl = toSqlKey tmplId
                mLineKey :: Maybe (Key Bomlinetmpl) = fmap toSqlKey lineId
            tmpl :: Bomtmpl <- runDbGet pool tmplKey
            let tmplEnt = Entity tmplKey tmpl


            html <- lift $ Lucid.renderTextT $ ViewBom.aditBomTemplate tmplEnt mLineKey mChildGroup pool isAuth
            S.html html

        S.post "/aditBomTemplate" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized" else do
                bomId :: Int64 <- S.param "id" 
                lineId :: Maybe Int64 <- getQueryParam "line" 
                posOnDrawing :: Text <- S.param "posOnDrawing"
                childTypeId :: Int64 <- S.param "childType"
                qty :: T.Text <- S.param "qty"
                mPropertyTypeTxt :: Maybe T.Text <- (fmap Just (S.param "property") `S.rescue` (const $ return Nothing))
                let mPropertyType :: Maybe PropertyType = (fmap T.unpack mPropertyTypeTxt) >>= readMaybe
                comment :: T.Text <- S.param "comment"

                let bomKey :: Key Bomtmpl = toSqlKey bomId
                    lineKey :: Maybe (Key Bomlinetmpl) = fmap toSqlKey lineId
                    childTypeKey :: Key ItemType = toSqlKey childTypeId
                    aditedLine = Bomlinetmpl bomKey (T.strip posOnDrawing) childTypeKey (T.strip qty) mPropertyType
                        (if T.length comment == 0 then Nothing else Just comment)
                    newLine Nothing = runDb pool (insert aditedLine)
                    newLine (Just lineKey) = runDb pool (SQL.replace lineKey aditedLine) >> return lineKey

                createdKey <- newLine lineKey

                params <- S.params
                let paramsParsed = filter $ fmap (\(str, value) -> (readMaybe $ TL.unpack str, value)) params
                        where filter ls = [ (key, value) | (Just key, value) <- ls ]

                let aditProperty (propId, value) = do 
                        filterEntity <- runDb pool $ getBy $ TmpllinePropertyUnique createdKey propId
                        updateDb (fmap entityKey filterEntity) createdKey propId (T.strip $ TL.toStrict value)
                            where
                                updateDb (Just key) _ _ "" = runDb pool $ delete key
                                updateDb (Just key) createdKey propId value =
                                    runDb pool $ SQL.replace key $ Propertyfilter createdKey propId value
                                updateDb Nothing _ _ "" = return ()
                                updateDb Nothing createdKey propId value =
                                    runDb pool $ insert_ $ Propertyfilter createdKey propId value


                mapM_ aditProperty paramsParsed

                S.redirect $ "/bomTemplate?id=" <> (TL.pack $ show bomId)

        S.post "/deleteBomTemplate" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized" else do
                bomId :: Int64 <- S.param "id" 
                lineId :: Int64 <- S.param "line" 

                let lineKey :: Key Bomlinetmpl = toSqlKey lineId

                runDb pool $ delete lineKey
                S.redirect $ "/bomTemplate?id=" <> (TL.pack $ show bomId)

        S.get "/items" $ do
            isAuth <- checkCredentials
            typeId :: Int64 <- S.param "type" 
            let key :: Key ItemType = toSqlKey typeId
            itemType :: ItemType <- runDbGet pool key
            html <- lift $ Lucid.renderTextT $ ViewItems.items (Entity key itemType) isAuth pool
            S.html html

        S.get "/checkBomBuild" $ do
            isAuth <- checkCredentials
            itemId :: Int64 <- S.param "item" 
            let itemKey :: Key Item = toSqlKey itemId
            item :: Item <- runDbGet pool itemKey
            html <- lift $ Lucid.renderTextT $ ViewBom.generatedBom (Entity itemKey item) pool isAuth
            S.html html

        S.get "/generateBom" $ do
            isAuth <- checkCredentials
            itemId :: Int64 <- S.param "item" 
            let itemKey :: Key Item = toSqlKey itemId
            item :: Item <- runDbGet pool itemKey
            print :: Maybe () <- (fmap Just (S.param "print") `S.rescue` (const $ return Nothing))

            params <- S.params
            let assemblingPlan::[(Key Bomlinetmpl, Maybe (Key Item), Key Item, T.Text)] =
                    mapMaybe
                        (\(bomLineIdTxt, itemIdTxt) -> f (parseKeyName bomLineIdTxt) (decimal itemIdTxt))
                        params
                    where
                        f ((Just bomKey), mItemKey) (Right (id2, action)) =  Just (bomKey, mItemKey, toSqlKey id2, TL.toStrict action)
                        f _ _ = Nothing


                        parseKeyName str = case decimal str of
                            Right (idBom, "") -> (Just (toSqlKey idBom), Nothing)
                            Right (idBom, str2) -> case decimal $ TL.tail str2 of
                                Right (idItem, "") -> (Just (toSqlKey idBom), Just (toSqlKey idItem))
                                _ -> (Nothing, Nothing)
                            Left _ ->  (Nothing, Nothing)
                            
            if isJust print then do
                html <- lift $ Lucid.renderTextT $ ViewBom.assemblingJobPrint (Entity itemKey item) assemblingPlan pool isAuth
                S.html html
            else do
                html <- lift $ Lucid.renderTextT $ ViewBom.assemblingJob (Entity itemKey item) assemblingPlan pool isAuth
                S.html html

        S.get "/linksToBoms" $ do
            isAuth <- checkCredentials
            itemId :: Int64 <- S.param "item" 
            let itemKey :: Key Item = toSqlKey itemId
            item :: Item <- runDbGet pool itemKey
            html <- lift $ Lucid.renderTextT $ ViewItems.item2bomsLinks (Entity itemKey item) isAuth pool
            S.html html

        S.post "/linksToBoms" $ do 
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                itemId :: Int64 <- S.param "item" 
                let itemKey :: Key Item = toSqlKey itemId

                params <- S.params
                let paramsG = groupBy (\(id1, _) (id2, _) -> id1 == id2) params

                let aditLink bomlineIdTxt = case (decimal $ fst $ head bomlineIdTxt) of
                        Right (bomlineId, _) -> do
                            let bomlineKey :: Key Bomlinetmpl = toSqlKey bomlineId
                            linkEntity <- runDb pool $ getBy $ ItemBomLinkUnique itemKey bomlineKey
                            updateDb (fmap entityKey linkEntity) itemKey bomlineKey toCreate
                                where
                                    updateDb (Just key) _ _ False = runDb pool $ delete key
                                    updateDb (Just key) itemKey bomlineKey True = return ()
                                    updateDb Nothing _ _ False = return ()
                                    updateDb Nothing itemKey bomlineKey True =
                                        runDb pool $ insert_ $ ItemBomLink itemKey bomlineKey
                                    toCreate = (Prelude.length bomlineIdTxt) == 2 

                        Left _ -> return ()

                mapM_ aditLink paramsG

                item <- runDb pool $ getJust itemKey
                planItem <- runDb pool $ getJust (itemPlanItem item)
                S.redirect $ "/items?type=" <> (TL.pack $ show $ fromSqlKey $ planItemItemType planItem)

        S.get "/planItems" $ do
            isAuth <- checkCredentials
            typeId :: Int64 <- S.param "type" 
            let key :: Key ItemType = toSqlKey typeId
            itemType :: ItemType <- runDbGet pool key
            let filterVal = toPersistValue typeId
            res :: [(Entity PlanItem, Single Int64, Single Int64, Single Int64, Maybe (Single Int64), Maybe (Single Int64))] <- runDb pool (rawSql planItemsStatsSql [filterVal] )
            S.html $ TL.pack $ R.renderHtml $ templatePage $ planItemsPage (Entity key itemType) res isAuth

        S.get "/stock" $ do 
            itemId :: Int64 <- S.param "id" 
            let itemKey :: Key Item = toSqlKey itemId
            item <- runDbGet pool itemKey
            onOrder :: [Entity OnOrder] <- runDb pool $ rawSql stockOnOrderSql [toPersistValue itemId] 
            onHand :: [Entity OnHand] <- runDb pool $ rawSql stockOnHandSql [toPersistValue itemId] 
            S.html $ TL.pack $ R.renderHtml $ templatePage $ stockPage (Entity itemKey item) onOrder onHand

        S.get "/editProperties" $ do 
            isAuth <- checkCredentials
            if isAuth then do
                propertyTypeTxt :: T.Text <- S.param "property" 
                let propertyType :: PropertyType = read $ T.unpack propertyTypeTxt
                typeId :: Int64 <- S.param "type" 
                let typeKey :: Key ItemType = toSqlKey typeId
                itemType <- runDbGet pool typeKey
                html <- lift $ Lucid.renderTextT $ ViewItems.editProperties
                                    (Entity typeKey itemType) propertyType pool
                S.html html
            else  S.text "Oh, no!"


        S.post "/editProperties" $ do 
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                propertyTypeTxt :: T.Text <- S.param "property" 
                let propertyType :: PropertyType = read $ T.unpack propertyTypeTxt
                params <- S.params

                let aditProperty (itemIdTxt, value) = case (decimal itemIdTxt) of
                        Right (itemId, _) -> do
                            let itemKey :: Key Item = toSqlKey itemId
                            propertyEntity <- runDb pool $ getBy $ ItemPropertyUnique itemKey propertyType
                            updateDb (fmap entityKey propertyEntity) itemKey propertyType (T.strip $ TL.toStrict value)
                                where
                                    updateDb (Just key) _ _ "" = runDb pool $ delete key
                                    updateDb (Just key) itemKey propertyType value =
                                        runDb pool $ SQL.replace key $ ItemProperty itemKey propertyType value
                                    updateDb Nothing _ _ "" = return ()
                                    updateDb Nothing itemKey propertyType value =
                                        runDb pool $ insert_ $ ItemProperty itemKey propertyType value

                        Left _ -> return ()

                mapM_ aditProperty params
                typeId :: TL.Text <- S.param "type"
                S.redirect $ "/items?type=" <> typeId

        S.get "/stockuploads" $ do 
            isAuth <- checkCredentials
            uploads :: [(Entity StockUpload, Single Int64, Single Int64)] <- runDb pool $ rawSql uploadsSql []
            S.html $ Lucid.renderText $ VL.templatePage $ VL.uploadsPage uploads isAuth

        S.post "/stockuploads" $ do 
            isAuth <- checkCredentials
            if isAuth then do
                id :: Int64 <- S.param "idToDelete"
                let key :: Key StockUpload = toSqlKey id
                runDb pool $ deleteWhere [OnOrderUpload ==. key]
                runDb pool $ deleteWhere [OnHandUpload ==. key]
                runDb pool $ delete key
                S.redirect "/stockuploads"
            else  S.text "Oh, no!"

        S.get "/upload" $ do 
            S.html $ TL.pack $ R.renderHtml $ templatePage $ uploadPage

        S.post "/upload" $ do 
            isAuth <- checkCredentials
            if (not isAuth)
                then 
                    S.raise "Not authorized!"
                else do
                    date :: Day <- (S.param "date" >>= parseTimeM True defaultTimeLocale "%Y-%-m-%-d")
                    files :: [(TL.Text, FileInfo BL.ByteString)] <- S.files
                    let csvDataOnHand :: BL.ByteString = fileContent $ snd $ head files
                    let csvDataOnOrder :: BL.ByteString = fileContent $ snd $ head $ tail files

                    let prepareOo itemId r = (\uploadKey ->
                                OnOrder uploadKey (Csv.rfq r) (Csv.rfqDate r) (toSqlKey $ unSingle itemId)
                                    (Csv.getDouble $ Csv.qtyOnOrder r) (Csv.getDouble $ Csv.price r) (Csv.eta r))
                    let putStatusOmit groupId r
                            | (T.length $ snd $ breakOn "scrap" (toLower $ Csv.placeName r)) /= 0  = "Omit"
                            | (T.length $ snd $ breakOn "scrap" (toLower $ Csv.ubication r)) /= 0  = "Omit"
                            | (T.length $ snd $ breakOn "FS-" $ Csv.placeName r) /= 0  = "Omit"
                            | (T.length $ snd $ breakOn "-Workshop" $ Csv.placeName r) /= 0 &&
                                    (T.strip $ Csv.status1c r) == "" &&
                                    ((T.strip $ Csv.ubication r) == "" || (T.strip $ Csv.ubication r) == "1") = "Used"
                            | (T.length $ snd $ breakOn "Borets" (Csv.owner r)) == 0 && getGroupName groupId == "Cable" = "Omit"
                            | (T.length $ snd $ breakOn "Borets" (Csv.owner r)) == 0 && getGroupName groupId == "Sensor" = "Omit"
                            | (T.length $ snd $ breakOn "Borets" (Csv.owner r)) == 0 && getGroupName groupId == "SF" = "Omit"
                            | otherwise = Csv.getStatus $ Csv.status r

                    let prepareOh itemId groupId r = (\uploadKey -> 
                            OnHand uploadKey (toSqlKey $ unSingle itemId) "" (Csv.placeName r) (Csv.ubication r)
                            (Csv.owner r) (Csv.getDouble $ Csv.qtyOnHand r) (Csv.status1c r)  (putStatusOmit (unSingle groupId) r))

                    let prepareRecordsOo ldata r f = case  lookup (Single $ Csv.code1c r) ldata of
                            Nothing -> (r : (fst f), snd f)
                            Just (itemId, _) -> (fst f, (prepareOo itemId r):(snd f))
                    let prepareRecordsOh ldata r f = case  lookup (Single $ Csv.code1c r) ldata of
                            Nothing -> (r : (fst f), snd f)
                            Just (itemId, groupId) -> (fst f, (prepareOh itemId groupId r):(snd f))
                        
                    let parseFiles = do 
                        { parsedDataOnHand :: (Csv.Header, V.Vector Csv.OnHand)
                            <- ExceptT $ return $ Csv.decodeByNameWith Csv.decodeOptions csvDataOnHand
                        ; parsedDataOnOrder :: (Csv.Header, V.Vector Csv.OnOrder)
                            <- ExceptT $ return $ Csv.decodeByNameWith Csv.decodeOptions csvDataOnOrder

                        ; lookupData :: [(Single Int, (Single Int64, Single Text))] <- lift $ runDb pool $ rawSql Csv.codes1cLookupSql []
                        ; let checkOnHand l = foldr (\x y -> prepareRecordsOh lookupData x y) ([],[]) (V.toList l)
                        ; let checkOnOrder l = foldr (\x y -> prepareRecordsOo lookupData x y) ([],[]) (V.toList l)
                        ; let (onHandErrors, onHandData) = checkOnHand $ snd parsedDataOnHand
                        ; let (onOrderErrors, onOrderData) = checkOnOrder $ snd parsedDataOnOrder
                        
                        ; if (null onHandErrors) && (null onOrderErrors) then do
                        ;   uploadKey <- runDb pool $ insert $ StockUpload date ""
                        ;    runDb pool $ insertMany_ (map (\template -> template uploadKey) onOrderData)
                        ;    runDb pool $ insertMany_ (map (\template -> template uploadKey) onHandData)
                        ;    return $ S.redirect "/stockuploads"
                        ; else return $ S.html $ TL.pack $ R.renderHtml $ templatePage $ uploadErrorsPage onOrderErrors onHandErrors
                        }
                        
                    result <- runExceptT parseFiles
                    either (S.raise . TL.pack) id result

        S.get "/aditType" $ do 
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                typeId :: Int64 <- S.param "id" `S.rescue` (const S.next)
                let key :: Key ItemType = toSqlKey typeId
                itemType :: ItemType <- runDbGet pool key
                S.html $ Lucid.renderText $ VL.templatePage $ VL.aditTypePage (Right $ Entity key itemType)

        S.get "/aditType" $ do 
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                group :: Text <- S.param "group"
                S.html $ Lucid.renderText $ VL.templatePage $ VL.aditTypePage (Left group)

        S.post "/aditType" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                mTypeId :: Maybe Int64 <- getQueryParam "id"
                name :: Text <- S.param "name" 
                group :: Text <- S.param "group" 
                uom :: Text <- S.param "uom" 
                recuperation :: Double <- S.param "recuperation" 
                custcode :: Text <- S.param "custcode" 
                custduty :: Double <- S.param "custduty" 
                note :: Text <- S.param "note" 
                repworkRuleStr :: String <- S.param "repworkrule"
                let repworkRule :: Maybe RepworkRule = readMaybe repworkRuleStr
                    newType = ItemType name group uom  "" recuperation "" False custcode custduty note repworkRule
                key <- case mTypeId of
                    Just typeId ->
                        let typeKey :: Key ItemType = toSqlKey typeId
                        in (runDb pool $ SQL.replace typeKey newType) >> return typeKey
                    Nothing -> runDb pool (insert newType)
                S.redirect $  TL.pack $ "/group?id=" ++ T.unpack group

        S.post "/deleteType" $ do
            isAuth <- checkCredentials
            if  isAuth then do
                typeId :: Int64 <- S.param "id" 
                let key :: Key ItemType = toSqlKey typeId
                itemType :: ItemType <- runDbGet pool key
                runDb pool (delete key)
                S.redirect $  TL.pack $ "/group?id=" ++ T.unpack (itemTypeGroup itemType)
            else S.text "Oh, no!"

        S.get "/properties" $ do
            html <- lift $ Lucid.renderTextT $ VL.propertiesPage allPropertyTypes
            S.html html

        S.get "/editPlanItem" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                id :: Int64 <- S.param "id" `S.rescue` (const S.next)
                let key :: Key PlanItem = toSqlKey id
                planItem :: PlanItem <- runDbGet pool key
                itemTypes :: [Entity ItemType] <- runDb pool (selectList [] [Asc ItemTypeGroup, Asc ItemTypeName])
                S.html $ TL.pack $ R.renderHtml $ templatePage $ editPlanItem (Right $ Entity key planItem) itemTypes

        S.get "/editPlanItem" $ do 
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                itype :: Int64 <- S.param "itype"
                itemTypes :: [Entity ItemType] <- runDb pool (selectList [] [Asc ItemTypeGroup, Asc ItemTypeName])
                S.html $ TL.pack $ R.renderHtml $ templatePage $ editPlanItem (Left itype) itemTypes

        S.post "/editPlanItem" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                name :: Text <- S.param "name" 
                price :: Double <- S.param "price" 
                itypeId :: Int64 <- S.param "itemType" 
                let itypeKey :: Key ItemType = toSqlKey itypeId
                id :: Int64 <- (S.param "id" `S.rescue` (const $ return 0))
                case id of
                    0 -> runDb pool (insert $ PlanItem itypeKey name price)
                    _ -> runDb pool updateQuery >> return key 
                        where
                            key :: Key PlanItem = toSqlKey id
                            updateQuery = update key [ PlanItemItemType =. itypeKey, PlanItemName =. name, PlanItemPrice =. price ]
                S.redirect $ TL.pack $ "/planItems?type=" ++ (show itypeId)

        S.post "/deletePlanItem" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                itemId :: Int64 <- S.param "id" 
                let key :: Key PlanItem = toSqlKey itemId
                planItem :: PlanItem <- runDbGet pool key
                runDb pool (delete key)
                S.redirect $ TL.pack $ "/planItems?type=" ++ (show $ fromSqlKey $ planItemItemType planItem)


        S.get "/planItem" $ do
            isAuth <- checkCredentials
            planItemId :: Int64 <- S.param "id" 
            let key :: Key PlanItem = toSqlKey planItemId
            planItem :: PlanItem <- runDbGet pool key
            itemType :: ItemType <- runDbGet pool (planItemItemType planItem)
            items :: [(Entity Item, Maybe (Single Double), Maybe (Single Double), Maybe (Single Double))] <- runDb pool $
                                        rawSql itemsQtySql [toPersistValue planItemId]

            children :: [(Entity PlanItem, Maybe (Entity Item), Single Double, Maybe (Single Double), Maybe (Single Double))] <- runDb pool $
                                        rawSql childrenSql2 [toPersistValue planItemId]

            let keyItemType :: Key ItemType = planItemItemType planItem
            S.html $ TL.pack $ R.renderHtml $ templatePage $ planItemPage ((Entity keyItemType itemType), (Entity key planItem)) items children isAuth

        S.get "/parents" $ do
            isAuth <- checkCredentials
            childId :: Int64 <- S.param "child" 
            let childKey :: Key PlanItem = toSqlKey childId
            planItem :: PlanItem <- runDbGet pool childKey
            itemType :: ItemType <- runDbGet pool $ planItemItemType planItem
            let keyItemType = planItemItemType planItem
            boms :: [(Entity ItemType, Entity PlanItem, Single Double)] <- runDb pool (rawSql parentsSql [toPersistValue childId])
            S.html $ TL.pack $ R.renderHtml $ templatePage $ parentsPage (Entity keyItemType itemType, Entity childKey planItem) boms isAuth

        S.get "/children" $ do
            isAuth <- checkCredentials
            parentId :: Int64 <- S.param "parent" 
            let parentKey :: Key PlanItem = toSqlKey parentId
            planItem :: PlanItem <- runDbGet pool parentKey
            itemType :: ItemType <- runDbGet pool $ planItemItemType planItem
            let keyItemType = planItemItemType planItem
            boms :: [(Single Int64, Entity ItemType, Entity PlanItem, Single Double)] <- runDb pool (rawSql childrenSql [toPersistValue parentId])
            S.html $ TL.pack $ R.renderHtml $ templatePage $ childrenPage (Entity keyItemType itemType, Entity parentKey planItem) boms isAuth

        S.get "/aditBom" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                parentId :: Int64 <- S.param "parent" 
                let parentKey :: Key PlanItem = toSqlKey parentId
                parent :: PlanItem <- runDbGet pool parentKey

                parentType :: ItemType <- runDbGet pool $ planItemItemType parent
                let parentTypeKey = planItemItemType parent

                mBomE <- runMaybeT $ do
                    bomId <- getQueryParamMT "bomId"
                    let bomKey :: Key Bom = toSqlKey bomId
                    bom <- runDbGetMT pool bomKey
                    return $ Entity bomKey bom
                
                mChildExtE <- runMaybeT $ do
                    bomE <- MaybeT $ return mBomE
                    let childKey = bomChild $ entityVal bomE
                    child <- runDbGetMT pool childKey
                    let childTypeKey = planItemItemType child
                    childType <- runDbGetMT pool childTypeKey
                    return (Entity childTypeKey childType, Entity childKey child)

                types :: [ Entity ItemType ] <- runDb pool $ selectList [] [Asc ItemTypeGroup, Asc ItemTypeName]

                mSelectedTypeE <- runMaybeT $ do
                    selectedTypeId <- getQueryParamMT "selectedType"
                    let selectedTypeKey = toSqlKey selectedTypeId
                    selectedType <- runDbGetMT pool selectedTypeKey
                    return $ Entity selectedTypeKey selectedType

                candidates :: [ Entity PlanItem ] <- 
                    ((runMaybeT $ do
                        selectedTypeE <- MaybeT $ return mSelectedTypeE
                        let selectedTypeKey = entityKey selectedTypeE
                        lift $ runDb pool $ selectList [PlanItemItemType ==. selectedTypeKey] [Asc PlanItemName])
                            >>= (\a -> return $ fromMaybe [] a))


                S.html $ TL.pack $ R.renderHtml $ templatePage $
                    aditChildPage (Entity parentTypeKey parentType, Entity parentKey parent)
                        mChildExtE mBomE mSelectedTypeE types candidates 


        S.post "/aditBom" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                parentId :: Int64 <- S.param "parent" 
                let parentKey :: Key PlanItem = toSqlKey parentId
                qty :: Double <- S.param "qty" 
                mChildId <- getQueryParam "child"
                case mChildId of
                    Just childId -> do
                        cycles :: [ Single Int64 ] <- runDb pool $ rawSql checkCyclesSql [toPersistValue childId, toPersistValue parentId]
                        if null cycles then do
                            let childKey = toSqlKey childId
                            mBomId <- getQueryParam "bomId"
                            case mBomId of
                                Just bomId ->
                                    let bomKey :: Key Bom = toSqlKey bomId
                                    in (runDb pool $ SQL.replace bomKey $ Bom parentKey childKey qty) >> return bomKey
                                Nothing -> runDb pool $ insert $ Bom parentKey childKey qty
                        else S.raise $ TL.pack $ unpack $ "Prohibited create cycles: " <> (pack $ show $ cycles)
                    Nothing -> S.raise $ TL.pack $ unpack $ "Child not found!"
                S.redirect ("/children?parent=" <> (TL.pack $ show parentId))
        
        S.get "/copyChildren" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                parentId :: Int64 <- S.param "parent" 
                let parentKey :: Key PlanItem = toSqlKey parentId
                parent :: PlanItem <- runDbGet pool parentKey

                parentType :: ItemType <- runDbGet pool $ planItemItemType parent
                let parentTypeKey = planItemItemType parent

                types :: [ Entity ItemType ] <- runDb pool $ selectList [] [Asc ItemTypeGroup, Asc ItemTypeName]

                mSelectedTypeE <- runMaybeT $ do
                    selectedTypeId <- getQueryParamMT "selectedType"
                    let selectedTypeKey = toSqlKey selectedTypeId
                    selectedType <- runDbGetMT pool selectedTypeKey
                    return $ Entity selectedTypeKey selectedType

                let selectedTypeE = fromMaybe (Entity parentTypeKey parentType) mSelectedTypeE
                candidates :: [ Entity PlanItem ] <- runDb pool $
                        selectList [PlanItemItemType ==. (entityKey selectedTypeE)] [Asc PlanItemName]


                S.html $ TL.pack $ R.renderHtml $ templatePage $
                    copyChildrenPage (Entity parentTypeKey parentType, Entity parentKey parent)
                        selectedTypeE types candidates 

        S.post "/copyChildren" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                parentId :: Int64 <- S.param "parent"
                let parentKey :: Key PlanItem = toSqlKey parentId
                mDonorId <- getQueryParam "donor"
                case mDonorId of
                    Just donorId -> do
                        cycles :: [ Single Int64 ] <- runDb pool $ rawSql checkCyclesSql [toPersistValue donorId, toPersistValue parentId]
                        if null cycles then do
                            candidates :: [(Single Int64, Entity PlanItem, Entity ItemType, Single Double)] <- runDb pool $ rawSql childrenSql [toPersistValue donorId]
                            let copyChild (_, eChild, _, qty) = runDb pool $ insert $ Bom parentKey (entityKey eChild) (unSingle qty)
                            mapM_ copyChild candidates

                        else S.raise $ TL.pack $ "Prohibited create cycles in BOMs:" ++ (show $ cycles)

                    Nothing -> S.raise $ "Donor not found!"
                S.redirect ("/children?parent=" <> (TL.pack $ show parentId))

        S.post "/deleteBom" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                childId :: Int64 <- S.param "child"
                let childKey :: Key PlanItem = toSqlKey childId
                parentId :: Int64 <- S.param "parent"
                let parentKey :: Key PlanItem = toSqlKey parentId
                runDb pool $ deleteBy $ ParentChild parentKey childKey
                redirectTo :: TL.Text <- S.param "redirectTo"
                S.redirect redirectTo

        S.get "/add2parents" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                planItemId :: Int64 <- S.param "child" 
                let key :: Key PlanItem = toSqlKey planItemId
                planItem :: PlanItem <- runDbGet pool key
                let keyItemType = planItemItemType planItem
                itemType :: ItemType <- runDbGet pool $ keyItemType
                types :: [ Entity ItemType ] <- runDb pool $ selectList [] [Asc ItemTypeGroup, Asc ItemTypeName]
                let defaultType = head types
                filteredTypeId :: Int64 <- (S.param "type" `S.rescue` (const $ return $ fromSqlKey $ entityKey defaultType))
                let filteredTypeKey :: Key ItemType = toSqlKey filteredTypeId
                filterName :: Text <- (S.param "filterName" `S.rescue` (const $ return ""))
                selectedType <- runDbGet pool filteredTypeKey
                candidates :: [(Entity PlanItem, Single Double)] <- runDb pool $ rawSql candidates2ParentsSql [toPersistValue planItemId, toPersistValue filteredTypeId, toSearchParam filterName]
                S.html $ TL.pack $ R.renderHtml $ templatePage $ add2parentsPage
                        (Entity keyItemType itemType, Entity key planItem)
                        types
                        (Entity filteredTypeKey selectedType, (unpack filterName), candidates)


        S.post "/add2parents" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                childId :: Int64 <- S.param "child" 
                let childKey :: Key PlanItem= toSqlKey childId
                qty :: Double <- S.param "qty"
                params <- S.params
                let addBom (key, _) = case (decimal key) of
                        Right (parentId, _) -> do
                            let parentKey :: Key PlanItem = toSqlKey parentId
                            cycles :: [ Single Int64 ] <- runDb pool $ rawSql checkCyclesSql [toPersistValue childId, toPersistValue parentId]
                            if null cycles then do
                                runDb pool $ insert $ Bom parentKey childKey qty
                            else S.raise $ TL.pack $ "Prohibited create cycles in BOMs: parent"
                                ++ (show parentId)
                                ++ ", child " ++ (show childId)
                                ++ ", cycle " ++ (show cycles)   
                        Left _ -> return $ toSqlKey 0
                mapM_ addBom params
                S.redirect $ TL.pack $ "/parents?child=" ++ (show childId)
            

        S.get "/aditItemOLD" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                mItemId <- getQueryParam "id"
                mItem :: Maybe Item <- runMaybeT $ (MaybeT $ return mItemId) >>= MaybeT . runDb pool . get . toSqlKey

                mPitemIdFromParam <- getQueryParam "planItem"
                let mPitemId = mPitemIdFromParam <|> fmap (fromSqlKey . itemPlanItem) mItem

                mPlanItem :: Maybe PlanItem <- runMaybeT $ MaybeT (return mPitemId) >>= MaybeT . runDb pool . get . toSqlKey
                mItypeIdFromParam <- getQueryParam "itemType"
                let mItypeId = mItypeIdFromParam <|> (fmap (fromSqlKey . planItemItemType) mPlanItem)

                types :: [Entity ItemType] <- runDb pool (selectList [] [Asc ItemTypeGroup, Asc ItemTypeName])
                pitems :: Maybe [Entity PlanItem] <- runMaybeT $ MaybeT (return mItypeId) >>= (\id -> lift $ runDb pool (selectList [PlanItemItemType ==. (toSqlKey id)] [Asc PlanItemName]))
                S.html $ TL.pack $ R.renderHtml $ templatePage $ aditItemPage (mItypeId, mPitemId, mItemId) mItem (types, fromMaybe [] pitems) 

        S.get "/aditItem" $ do
            isAuth <- checkCredentials
            unless isAuth S.finish

            mItemId <- getQueryParam "id"
            let mItemKey = fmap toSqlKey mItemId
            mItem :: Maybe Item <- runMaybeT $ (MaybeT $ return mItemKey) >>= MaybeT . runDb pool . get 

            mPlanItemId <- getQueryParam "planItem"
            let mPlanItemKey = fmap toSqlKey mPlanItemId
            mPlanItem :: Maybe PlanItem <- runMaybeT $ MaybeT (return mPlanItemKey) >>= MaybeT . runDb pool . get

            mItemTypeId <- getQueryParam "itemType"
            let mItemTypeKey = fmap toSqlKey mItemTypeId

            let iniConf (Just itemEnt) _ = ViewItems.EditItem itemEnt
                iniConf _ (Just planItemEnt) = ViewItems.CreateInPlanItem planItemEnt
                iniConf Nothing Nothing = ViewItems.Empty

            html <- lift $ Lucid.renderTextT $ ViewItems.aditItemPage mItemTypeKey (iniConf (Entity <$> mItemKey <*> mItem) (Entity <$> mPlanItemKey <*> mPlanItem)) isAuth pool
            S.html html

        S.post "/aditItem" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                redirectTo :: TL.Text <- S.param "redirectTo"

                planItemId :: Int64 <- S.param "planItem"
                let planItemKey :: Key PlanItem = toSqlKey planItemId
                code1c :: Int <- S.param "code1c" 
                pn :: Text <- S.param "pn" 
                descr :: Text <- S.param "description" 
                uom :: Text <- S.param "uom" 
                conversion :: Double <- S.param "conversion"
                price :: Double <- S.param "price" 
                isObsolete :: Bool <- (S.param "isObsolete" `S.rescue` (const $ return False))
                bomtmplId :: Maybe Int64 <- getQueryParam "bomtmpl" 
                let bomtmplKey :: Maybe (Key Bomtmpl) = fmap toSqlKey bomtmplId

                mItemId <- getQueryParam "id"
                case mItemId of
                    Just itemId -> do
                        let itemKey :: Key Item  = toSqlKey itemId
                        oldItem <- runDbGet pool itemKey
                        let newIsMain = if itemPlanItem oldItem == planItemKey then itemIsMain oldItem else False
                            newItem = Item code1c pn descr uom price 0 planItemKey conversion
                                            newIsMain isObsolete bomtmplKey
                        runDb pool (SQL.replace itemKey newItem) >> return itemKey
                    Nothing ->
                        let newItem = Item code1c pn descr uom price 0 planItemKey conversion False False Nothing
                        in runDb pool (insert newItem)

                S.redirect redirectTo

        S.post "/setMainItem" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                redirectTo :: TL.Text <- S.param "redirectTo"
                id :: Int64 <- S.param "id"
                let key :: Key Item = toSqlKey id
                item :: Item <- runDbGet pool key
                runDb pool $ updateWhere [ ItemPlanItem ==. (itemPlanItem item) ] [ ItemIsMain =. False ]
                runDb pool $ update key [ ItemIsMain =. True ]
                S.redirect redirectTo

        S.post "/deleteItem" $ do
            isAuth <- checkCredentials
            if (not isAuth) then S.raise "Not authorized"
            else do
                itemId :: Int64 <- S.param "id" 
                redirectTo :: TL.Text <- S.param "redirectTo"
                let key :: Key Item = toSqlKey itemId
                runDb pool (delete key)
                S.redirect redirectTo


        S.get "/checkCodes1C" $ do
                codes :: TL.Text <- S.param "codes" `S.rescue` (const S.next) 
                let codesParsed = map (\str -> either
                        (const $ Left $ TL.toStrict str)
                            (\(code::Int64, rest) -> if TL.length rest == 0 then Right code else Left $ TL.toStrict str)
                                (decimal str)) $ TL.words codes
                let searchItem parsedStr = case parsedStr of
                        Left str -> return $ Left str 
                        Right code -> (runDb pool (rawSql checkCode1cSql [toPersistValue code]))
                            >>= (\items ->
                                case items :: [(Entity Item, Entity PlanItem, Entity ItemType)] of
                                    item:_ -> return $ Right item
                                    _ -> return $ Left $ T.pack $ show code) 
                eItems <- mapM searchItem codesParsed
                S.html $ TL.pack $ R.renderHtml $ templatePage $ checkItemsResultsPage eItems

        S.get "/checkCodes1C" $ do
                S.html $ TL.pack $ R.renderHtml $ templatePage $ checkItemsPage 

        S.get "/excel/items" $ do
            list :: [(Entity Item, Entity PlanItem, Entity ItemType)] <- runDb pool (rawSql ForExcel.itemsSql [])
            S.setHeader "Content-Type" "text/xml; charset=utf-8"
            S.raw $ ForExcel.itemsXml list

        S.get "/excel/boms" $ do
            boms :: [(Entity Bom, Single Text, Single Text)] <- runDb pool (rawSql ForExcel.bomsSql [])
            S.setHeader "Content-Type" "text/xml; charset=utf-8"
            S.raw $ ForExcel.bomsXml boms 

        S.get "/excel/boms2" $ do
            items :: [Entity Item] <- runDb pool (selectList  [ItemBomtmpl !=. Nothing ][])
            bomslines :: [[BomFromTmpl]] <- mapM (\item -> ((liftIO $ ViewBom.bomGenerator item pool) )) items
            let boms = zip items bomslines
            S.setHeader "Content-Type" "text/xml; charset=utf-8"
            S.raw $ ForExcel.boms2Xml boms

        S.get "/excel/planItems" $ do
            planItems :: [(Entity PlanItem, Entity ItemType, Maybe (Entity Item))] <- runDb pool (rawSql ForExcel.planItemsSql [] )
            S.setHeader "Content-Type" "text/xml; charset=utf-8"
            S.raw $ ForExcel.planItemsXml planItems 

        S.get "/excel/stock" $ do
            date :: Day <- (S.param "date"
                        >>= parseTimeM True defaultTimeLocale "%Y-%-m-%-d")
    
            onOrder :: [(Entity Item, Entity OnOrder)] <- runDb pool (rawSql ForExcel.onOrderSql [toPersistValue date])
            onHand :: [(Entity Item, Entity OnHand)] <- runDb pool (rawSql ForExcel.onHandSql [toPersistValue date])
            S.setHeader "Content-Type" "text/xml; charset=utf-8"
            S.raw $ ForExcel.stockXml onOrder onHand
