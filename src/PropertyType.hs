{-# LANGUAGE TemplateHaskell #-}

module PropertyType where

import Database.Persist.TH

data PropertyType
        = Amps
        | CSmm
        | Design
        | Flow
        | HBBushingID
        | IDmm
        | Length
        | LengthMM
        | Material
        | MotorHead
        | ODmm
        | RotorsQty
        | Series
        | ShaftOD
        | ShaftStrength
        | Size
        | StagesQty
        | StagesStdQty
        | StagesTAQty
        | StageType
        | Tandem
        | Thread
        | Volts
        deriving (Show, Read, Eq, Ord, Enum, Bounded)
derivePersistField "PropertyType"

allPropertyTypes = [(minBound :: PropertyType) ..] 

data RepworkRule
    = CorrosionResistenceRR
    | CutLengthMMRR
    | CutLengthRR
    deriving (Show, Read, Eq, Ord, Enum, Bounded)
derivePersistField "RepworkRule"

allRepworkRules = [(minBound :: RepworkRule) ..]
