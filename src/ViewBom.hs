{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module ViewBom where

import Text.RawString.QQ (r)
import GHC.Int
import Data.Monoid ((<>))
import Control.Monad.IO.Class (liftIO)
import Data.List (groupBy, group, head, sort, find)
import Data.Maybe (fromMaybe, isJust, catMaybes)
import qualified Data.Text as T
import Data.Double.Conversion.Text (toFixed, toShortest)
import Data.Monoid (mempty)
import Lucid.Base
import Lucid.Html5
import Database.Persist.Postgresql

import Types
import PropertyType(PropertyType(..), allPropertyTypes)
import qualified ViewsLucid as VL
import Database.Persist.Sql (ConnectionPool)

import Control.Applicative((<|>))
import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Control.Monad.Trans (lift)

import qualified Parser as P

bomTemplates :: Entity ItemType -> ConnectionPool -> Bool -> HtmlT IO ()
bomTemplates itemType pool isAuth = VL.templatePage2 body
    where
        body = do
            templates :: [Entity Bomtmpl]
                <- liftIO $ runSqlPool (selectList [ BomtmplParenttype ==. (entityKey itemType)] [Asc BomtmplName] ) pool 

            h1_ $ do
                a_ [href_ ("/group?id=" <> (itemTypeGroup $ entityVal itemType))] $ toHtml (getGroupName $ itemTypeGroup $ entityVal itemType)
                toHtml (" // "::T.Text)
                toHtml $ (itemTypeName . entityVal) itemType
            div_ $ toHtml $ "UOM: " <> (itemTypeUom $ entityVal  itemType)
            div_ $ if isAuth
                        then a_ [href_ ("aditBomTemplate?type=" <> (T.pack $ show $ fromSqlKey $ entityKey itemType))]
                                $ "Add template"
                        else mempty
            form_ $      
                table_ $ do
                    tr_ $ do
                        th_ "id"
                        th_ "Name"
                        th_ "Items#"
                        th_ "Lines#"
                        if isAuth then th_ "Action" else mempty
                    tbody_ $ mapM_ itemLine templates
            where
                itemLine bom =
                    tr_ $ do 
                        td_ $ if isAuth
                            then a_ [href_ ("/editBomTemplate?id=" <> key)] $ toHtml key
                            else toHtml key 
                        td_ $ a_ [href_ ("/bomTemplate?id=" <> key)] $ (toHtml . bomtmplName . entityVal) bom 
                        td_ "TBD"
                        td_ "TBD"
                        if isAuth
                            then td_ $ do
                                button_
                                    [ type_ "submit"
                                    , formmethod_ "POST"
                                    , formaction_ "/deleteBomTemplate"
                                    , name_ "id"
                                    , value_ key
                                    ]
                                    "D"
                            else toHtml (""::String)
                    where  key = T.pack $ show $ fromSqlKey $ entityKey bom 

template :: Entity Bomtmpl -> ConnectionPool -> Bool -> HtmlT IO ()
template bomTmpl pool isAuth =
    let body = do
            let filterVal = toPersistValue $ entityKey bomTmpl
            bomlines :: [(Entity Bomlinetmpl, Entity ItemType,  Maybe (Entity Propertyfilter))]
                            <- liftIO $ runSqlPool (rawSql bomTmplSql [filterVal]) pool 
            let
                bomBlocks = groupBy (\(e1, _, _) (e2, _, _) -> e1 == e2) bomlines

                bomLineSmall propertyValue = do
                    td_ $ toHtml $ maybe (""::T.Text)  (T.pack . show . propertyfilterPropertyType . entityVal) propertyValue
                    td_ $ toHtml $ maybe (""::T.Text)  (propertyfilterValue . entityVal) propertyValue
                    toHtml (""::String)

                bomLineBig lines = do
                    tr_ $ do
                        td_ [rowspan_ spanValue] $ toHtml $ T.pack $ show $ fromSqlKey $ entityKey bomline
                        td_ [rowspan_ spanValue] $ toHtml $ itemTypeName$ entityVal itemType
                        td_ [rowspan_ spanValue] $ toHtml $ bomlinetmplPosOnDrawing $ entityVal bomline
                        td_ [rowspan_ spanValue] $ toHtml $ showQty 
                        td_ [rowspan_ spanValue] $ toHtml $ fromMaybe "" $ bomlinetmplComment $ entityVal bomline
                        (bomLineSmall $ head propertyFilters) 
                        if isAuth
                            then td_ [rowspan_ spanValue] $ do
                                button_
                                    [ type_ "submit"
                                    , formmethod_ "GET"
                                    , formaction_ "/aditBomTemplate"
                                    , name_ "line"
                                    , value_ (T.pack $ show $ fromSqlKey $ entityKey bomline)
                                    ]
                                    "E"
                                button_
                                    [ type_ "submit"
                                    , formmethod_ "POST"
                                    , formaction_ "/deleteBomTemplate"
                                    , name_ "line"
                                    , value_ (T.pack $ show $ fromSqlKey $ entityKey bomline)
                                    ]
                                    "D"
                            else mempty
                    (mapM_ (tr_ . bomLineSmall) $ tail propertyFilters)    
                    toHtml (""::String)
                    where
                        bomline = (\(a, _, _) -> a) $ head lines
                        itemType = (\(_, a, _) ->a) $ head lines
                        propertyFilters = fmap (\(_, _, propertyFilter) -> propertyFilter) lines
                        spanValue = T.pack $ show $ length lines
                        showQty = (bomlinetmplQty $ entityVal bomline) <>
                                    (maybe "" (\pr -> "[" <> (T.pack $ show pr) <> "]") $ bomlinetmplPropertyType $ entityVal bomline)

            h1_ $ toHtml $ "Bom template - " <> (bomtmplName $ entityVal bomTmpl)
            div_ $ if isAuth
                then a_ [ href_ ("/aditBomTemplate?id=" <> (T.pack $ show $ fromSqlKey $ entityKey bomTmpl))]
                        $ "Add element"
                else toHtml (""::T.Text)
            form_ $ do
                input_ [type_ "hidden", name_ "id", value_ (T.pack $ show $ fromSqlKey $ entityKey bomTmpl)]
                table_ $ do
                    tr_ $ do
                        th_ "id"
                        th_ "Type"
                        th_ "Drawing Pos"
                        th_ "Qty"
                        th_ "Comment"
                        th_ "Property"
                        th_ "Value"
                        if isAuth then th_ "Action" else toHtml (""::T.Text)
                    tbody_ $ mapM_ bomLineBig bomBlocks

    in VL.templatePage2 body


aditBomTemplate :: Entity Bomtmpl -> Maybe (Key Bomlinetmpl) -> Maybe Int64 -> ConnectionPool -> Bool -> HtmlT IO ()
aditBomTemplate bom mBomLineKey mSelectedGroup pool isAuth =
    let body = do
            bomLine :: Maybe Bomlinetmpl <- runMaybeT
                $ MaybeT (return mBomLineKey) >>= (\key -> MaybeT $ liftIO $ runSqlPool (get key) pool)
            
            childType :: Maybe ItemType <- runMaybeT
                $ MaybeT (return $ fmap bomlinetmplChildtype bomLine) >>= (\key -> MaybeT $ liftIO $ runSqlPool (get key) pool)

            let childGroup = fromMaybe "" $ (fmap (T.pack . show) mSelectedGroup) <|> (fmap itemTypeGroup childType)
            types :: [Entity ItemType] <- liftIO $ runSqlPool (selectList [ItemTypeGroup ==. childGroup] [Asc ItemTypeName]) pool

            let properties4Select = fmap
                    (\p -> (T.pack $ show p, T.pack $ show p))
                    allPropertyTypes

            initialValues :: [Entity Propertyfilter] <- maybe
                (return [])
                (\line -> liftIO $ runSqlPool (selectList [PropertyfilterTmplLine ==. line][]) pool)
                mBomLineKey

            let initialValue prop = fromMaybe ""
                    $ fmap (propertyfilterValue . entityVal)
                        $ find (\iv -> (propertyfilterPropertyType $ entityVal iv) == prop) initialValues

                propertyFilterLine propertyType = do
                    div_ [ class_ "line" ] $ do
                        label_ $ toHtml $ T.pack $ show $ propertyType
                        input_ [ form_ "save", type_ "text" , name_ (T.pack $ show $ propertyType)
                                , value_ $ initialValue propertyType ]
                    toHtml (""::T.Text)

                    
            form_ [ id_ "get", method_ "GET", action_ "/aditBomTemplate" ] $ do
                input_ [ type_ "hidden", name_ "id",
                    value_ $ (T.pack . show . fromSqlKey . entityKey) bom ]
                input_ [ type_ "hidden", name_ "line",
                    value_ $ maybe "" (T.pack . show . fromSqlKey) mBomLineKey ]

            form_ [ id_ "save", method_ "POST", action_ "/aditBomTemplate" ] $ do
                input_ [ type_ "hidden", name_ "id",
                    value_ $ (T.pack . show . fromSqlKey . entityKey) bom ]
                input_ [ type_ "hidden", name_ "line",
                    value_ $ maybe "" (T.pack . show . fromSqlKey) mBomLineKey ]

            h1_ $ toHtml
                $ (if isJust mBomLineKey then "Edit line of BOM: " else "Add line to BOM: ")
                <> (bomtmplName $ entityVal $ bom)
            fieldset_ $ do

                div_ [ class_ "line" ] $ do
                    label_ "Group"
                    VL.selectHtml2
                        [ form_ "get", name_ "childGroup", required_ ""]
                        (fmap (\(a, b) -> (T.pack a, T.pack b)) groups)
                        $ childGroup

                    div_ $
                        button_ [ type_ "submit", form_ "get" ] "Send"

                div_ [ class_ "line" ] $ do
                    label_ "Item type"
                    VL.selectHtml2
                        [ form_ "save", name_ "childType", required_ ""]
                        (fmap (\e -> (itemTypeName $ entityVal e, T.pack $ show $ fromSqlKey $entityKey e)) types)
                        $ maybe "" (T.pack . show . fromSqlKey . bomlinetmplChildtype) bomLine 

                div_ [ class_ "line" ] $ do
                    label_ "Position on Drawing"
                    input_ [ form_ "save", type_ "text" , name_ "posOnDrawing", required_ "",
                            value_ $ maybe "" bomlinetmplPosOnDrawing bomLine ]

                div_ [ class_ "line" ] $ do
                    label_ "Quantity"
                    input_ [ form_ "save", type_ "text" , name_ "qty", required_ "",
                            value_ $ maybe "" bomlinetmplQty bomLine ]

                div_ [ class_ "line" ] $ do
                    label_ "Property"
                    VL.selectHtml2
                        [ form_ "save", name_ "property" ]
                        properties4Select
                        $ fromMaybe "" $ bomLine >>= bomlinetmplPropertyType >>= return . T.pack . show

                div_ [ class_ "line" ] $ do
                    label_ "Comment"
                    input_ [ form_ "save", type_ "text" , name_ "comment",
                            value_ $ fromMaybe "" $ bomLine >>= bomlinetmplComment ]

                (mapM_ propertyFilterLine allPropertyTypes)

                div_ [ class_ "buttons" ] $ do
                    button_ [form_ "save"] "Save"
                    button_ [form_ "save", type_ "reset"] "Reset"


    in VL.templatePage2 body

searchReplacement :: Entity Item -> ConnectionPool -> IO [Entity Item]
searchReplacement itemEnt pool = do
    let item = entityVal itemEnt
    planItem :: PlanItem <- runSqlPool (getJust $ itemPlanItem item) pool
    itemType :: ItemType <- runSqlPool (getJust $ planItemItemType planItem) pool
    itemsForFiltering :: [(Entity Item, Entity ItemProperty)] <-
        runSqlPool
            (rawSql itemsForFilteringSql [toPersistValue $ planItemItemType $ planItem])
            pool
    let candidates = groupBy (\(e1, _) (e2, _) -> e1 == e2) itemsForFiltering
        iprop2tuple ipropEnt = (itemPropertyPropertyType $ entityVal ipropEnt, itemPropertyValue $ entityVal ipropEnt)
        candidates2 = fmap (\props -> (fst $ head props, fmap (iprop2tuple . snd) props)) $ candidates
        itemProps = fmap (iprop2tuple . snd)
            $ filter (\(e, _) -> itemEnt == e)
                itemsForFiltering
        rpwRuleFunc = repworkRuleFuncSelector $ itemTypeRepworkRule itemType


    return $ fmap fst $ filter (\(candEnt, candProps) -> (candEnt /= itemEnt) && (rpwRuleFunc itemProps candProps)) $ candidates2

bomGenerator :: Entity Item -> ConnectionPool -> IO [BomFromTmpl]
bomGenerator itemEnt pool = do 
    let item = entityVal itemEnt
        itemKey = entityKey itemEnt
        mBomtmplKey = itemBomtmpl item
        searchItems tmpllineEnt parentProperties = do
            filters :: [Entity Propertyfilter] <-
                runSqlPool
                    (selectList [PropertyfilterTmplLine ==. entityKey tmpllineEnt][])
                    pool

            let filtersParsed :: [(PropertyType, T.Text)] = fmap calcFilterVal filters where
                    calcFilterVal filterEnt =
                        ( propertyfilterPropertyType $ entityVal filterEnt
                        , P.parseAndEvalProp
                            (propertyfilterValue $ entityVal filterEnt)
                            (maybe "NotFound" (itemPropertyValue . entityVal) parentProp)
                        )
                        where
                            parentProp = find
                                (\prop ->
                                    (itemPropertyPropertyType $ entityVal prop)
                                    ==
                                    (propertyfilterPropertyType $ entityVal filterEnt)
                                )
                                parentProperties
                        
            if null filters then do

                runSqlPool (rawSql itemsForBomLineSql [toPersistValue $ entityKey tmpllineEnt]) pool :: IO [Entity Item] 

            else do

                itemsForFiltering :: [(Entity Item, Entity ItemProperty)] <-
                    runSqlPool
                        (rawSql itemsForFilteringSql [toPersistValue $ bomlinetmplChildtype $ entityVal tmpllineEnt])
                        pool
                let itemsForFilteringGrouped = groupBy (\(e1, _) (e2, _) -> e1 == e2) itemsForFiltering
                    itemsFiltered = filter checkCandidate itemsForFilteringGrouped
                        where
                            checkCandidate candidate = all checkFilter filtersParsed
                                where
                                    checkFilter (propertyType, filterVal) = isJust $ find
                                        (\(_, prop) ->
                                        (itemPropertyPropertyType (entityVal prop) == propertyType)
                                        &&
                                        (itemPropertyValue (entityVal prop) == filterVal))
                                        (candidate)
                            
                return $ fmap (fst . head) itemsFiltered

    result <- runMaybeT $ do
        bomtmplKey <- MaybeT $ return mBomtmplKey
        parentProperties :: [Entity ItemProperty] <-
            liftIO $ runSqlPool
                (selectList [ItemPropertyItem ==. (entityKey itemEnt)][])
                pool
        bomtmpl :: Bomtmpl <- liftIO $ runSqlPool (getJust bomtmplKey) pool

        bomTmpllinesUnfolded :: [(Entity Bomlinetmpl, Entity ItemType,  Maybe (Entity Propertyfilter))]
                            <- liftIO $ runSqlPool (rawSql bomTmplSql [toPersistValue bomtmplKey]) pool 
        let bomTmpllines = groupBy (\(e1, _, _) (e2, _, _) -> e1 == e2) bomTmpllinesUnfolded

            generatedLine block  = do

                let (tmpllineEnt, itemType, _)  = head block

                filters :: [Entity Propertyfilter] <-
                    runSqlPool
                        (selectList [PropertyfilterTmplLine ==. entityKey tmpllineEnt][])
                        pool

                let filtersParsed :: [(PropertyType, T.Text)] = fmap calcFilterVal filters where
                        calcFilterVal filterEnt =
                            ( propertyfilterPropertyType $ entityVal filterEnt
                            , P.parseAndEvalProp
                                (propertyfilterValue $ entityVal filterEnt)
                                (maybe "NotFound" (itemPropertyValue . entityVal) parentProp)
                            )
                            where
                                parentProp = find
                                    (\prop ->
                                        (itemPropertyPropertyType $ entityVal prop)
                                        ==
                                        (propertyfilterPropertyType $ entityVal filterEnt)
                                    )
                                    parentProperties

                    qty = do
                        let qtyStr2parse = bomlinetmplQty $ entityVal tmpllineEnt
                            mPropertyMasterKey = bomlinetmplPropertyType $ entityVal tmpllineEnt
                            mPropertyMasterVal = mPropertyMasterKey
                                >>= (\key -> find (\propEnt -> key == (itemPropertyPropertyType $ entityVal propEnt)) parentProperties)
                                >>=  return . itemPropertyValue . entityVal
                        return $ P.calculate qtyStr2parse (fmap (read . T.unpack) mPropertyMasterVal)

                    items = do 

                        if null filters then do
                            runSqlPool
                                (rawSql itemsForBomLineSql [toPersistValue $ entityKey tmpllineEnt])
                                pool
                            :: IO [Entity Item] 
                        else do
                            itemsForFiltering :: [(Entity Item, Entity ItemProperty)] <-
                                runSqlPool
                                    (rawSql itemsForFilteringSql [toPersistValue $ bomlinetmplChildtype $ entityVal tmpllineEnt])
                                    pool
                            let itemsForFilteringGrouped = groupBy (\(e1, _) (e2, _) -> e1 == e2) itemsForFiltering
                                itemsFiltered = filter checkCandidate itemsForFilteringGrouped
                                    where
                                        checkCandidate candidate = all checkFilter filtersParsed
                                            where
                                                checkFilter (propertyType, filterVal) = isJust $ find
                                                    (\(_, prop) ->
                                                    (itemPropertyPropertyType (entityVal prop) == propertyType)
                                                    &&
                                                    (itemPropertyValue (entityVal prop) == filterVal))
                                                    (candidate)
                                        
                            return $ fmap (fst . head) itemsFiltered

                fmap (BomFromTmpl itemType tmpllineEnt (fromMaybe (-777) qty) filtersParsed) items

        lift $ mapM generatedLine bomTmpllines

    return $ fromMaybe [] result


generatedBom :: Entity Item -> ConnectionPool -> Bool -> HtmlT IO ()
generatedBom itemEnt pool isAuth = VL.templatePage2 body where
    body = do 
        let item = entityVal itemEnt
            itemKey = entityKey itemEnt
            mBomtmplKey = itemBomtmpl item
            showProperty itemPropertyEnt =
                tr_ $ do
                    td_ $ toHtml $ show $ itemPropertyPropertyType $ entityVal itemPropertyEnt
                    td_ $ toHtml $ itemPropertyValue $ entityVal itemPropertyEnt
                    toHtml (""::T.Text)
            showBomItem itemEnt = do
                    td_ $ toHtml $ T.pack $ show $ itemCode1C $ entityVal itemEnt
                    td_ $ toHtml $ itemPn $ entityVal itemEnt
                    td_ $ toHtml $ itemDescription $ entityVal itemEnt
                    td_ $ toHtml $ itemUom $ entityVal itemEnt
                    toHtml (""::T.Text)
            showBomLine bomLine = do
                tr_ $ do
                    td_ [rowspan_ spanVal] $ toHtml $ itemTypeName $ entityVal $ lineType bomLine
                    td_ [rowspan_ spanVal] $ toHtml $ itemTypeUom $ entityVal $ lineType bomLine
                    td_ [rowspan_ spanVal] $ toHtml $ bomlinetmplPosOnDrawing $ entityVal bomlinetmplEnt
                    td_ [rowspan_ spanVal] $ toHtml $ T.pack $ show $ evalQty bomLine
                    td_ [rowspan_ spanVal] $ toHtml $ fromMaybe "" $ bomlinetmplComment $ entityVal bomlinetmplEnt
                    td_ [rowspan_ spanVal] $ toHtml $ T.unwords $ fmap snd $ filtersParsed bomLine

                    if (length $ items bomLine) > 0 then
                        showBomItem $ head $ items bomLine 
                    else
                        toHtml (""::T.Text)
                if (length $ items bomLine) > 0 then
                    mapM_ (\i -> tr_ $ showBomItem i) $ tail $ items bomLine 
                else
                    toHtml (""::T.Text)
                where
                    bomlinetmplEnt = bomlineTmpl bomLine
                    spanVal = T.pack $ show $ max 1 $ length $ items bomLine

        parentProperties :: [Entity ItemProperty] <-
            liftIO $ runSqlPool (rawSql itemPropertiesExtSql [toPersistValue itemKey]) pool
        bomlines <- liftIO $ bomGenerator itemEnt pool


        h1_ $ toHtml $ (T.pack $ show $ itemCode1C item) <> "-" <> (itemPn item) <> "-" <> (itemDescription item)
        h2_ "Properties"
        table_ $ do
            tr_ $ do
                th_ "Property"
                th_ "Value"
            tbody_ $ mapM_ showProperty parentProperties
        h2_ $ "Bom"
        table_ $ do
            tr_ $ do
                th_ "Type"
                th_ "UOM"
                th_ "On Drawing#"
                th_ "Qty"
                th_ "Comment"
                th_ "Filters"
                th_ "Code 1C"
                th_ "PN"
                th_ "Description"
                th_ "UOM"
            tbody_ $ mapM_ showBomLine bomlines
        

assemblingJob2 :: Entity Item -> ConnectionPool -> Bool -> HtmlT IO ()
assemblingJob2 itemEnt pool isAuth = VL.templatePage2 body where
    body = do 
        let item = entityVal itemEnt
            itemKey = entityKey itemEnt
            mBomtmplKey = itemBomtmpl item
            showBomItem itemEnt = do
                    ohQty :: [Maybe (Single Double)] <- liftIO $ runSqlPool (rawSql stockOnHandSql [toPersistValue $ entityKey itemEnt]) pool
                    td_ $ input_ [type_ "radio"]
                    td_ $ toHtml $ T.pack $ show $ itemCode1C $ entityVal itemEnt
                    td_ $ toHtml $ itemPn $ entityVal itemEnt
                    with (td_ $ toHtml $ itemDescription $ entityVal itemEnt) (if (itemIsObsolete $ entityVal itemEnt) then [class_ "selected"] else [])
                    td_ $ toHtml $ itemUom $ entityVal itemEnt
                    td_ $ toHtml $ show $ itemConversion $ entityVal itemEnt
                    td_ $ toHtml $ maybe "" (show . unSingle) $ head ohQty
                    toHtml (""::T.Text)
            showBomLine bomLine =
                if evalQty bomLine /= 0 then do
                    tr_ $ do
                        td_ [rowspan_ spanVal] $ toHtml $ itemTypeName $ entityVal $ lineType bomLine
                        td_ [rowspan_ spanVal] $ toHtml $ bomlinetmplPosOnDrawing $ entityVal bomlinetmplEnt

                        if (length $ items bomLine) > 0 then
                            showBomItem $ head $ items bomLine 
                        else
                            toHtml (""::T.Text)
                    if (length $ items bomLine) > 0 then
                        mapM_ (\i -> tr_ $ showBomItem i) $ tail $ items bomLine 
                    else
                        toHtml (""::T.Text)
                else mempty
                where
                    bomlinetmplEnt = bomlineTmpl bomLine
                    spanVal = T.pack $ show $ max 1 $ length $ items bomLine

        bomlines <- liftIO $ bomGenerator itemEnt pool


        h1_ $ toHtml $ (T.pack $ show $ itemCode1C item) <> "-" <> (itemPn item) <> "-" <> (itemDescription item)
        h2_ $ "Prepare assembling job"
        table_ $ do
            tr_ $ do
                th_ "Type"
                th_ "On Drawing#"
                th_ "Select"
                th_ "Code 1C"
                th_ "PN"
                th_ "Description"
                th_ "UOM"
                th_ "Qty required"
                th_ "Qty on hand"
            tbody_ $ mapM_ showBomLine bomlines

assemblingJob :: Entity Item -> [(Key Bomlinetmpl, Maybe (Key Item),  Key Item, T.Text)] -> ConnectionPool -> Bool -> HtmlT IO ()
assemblingJob itemEnt asmPlan pool isAuth = VL.templatePage2 body where
    body = do 
        let item = entityVal itemEnt
            itemKey = entityKey itemEnt
            mBomtmplKey = itemBomtmpl item
            showBomItem level lineNum bomLine mRepworkItemKey itemEnt = with (tr_ $ do
                ohQty :: [Maybe (Single Double)] <- liftIO $ runSqlPool (rawSql stockOnHandSql [toPersistValue $ entityKey itemEnt]) pool
                ooQty :: [Maybe (Single Double)] <- liftIO $ runSqlPool (rawSql stockOnOrderSql [toPersistValue $ entityKey itemEnt]) pool
                let
                    assembled = case find (\(bomKey, _, itemKey, op) -> entityKey itemEnt == itemKey && op == "asm") asmPlan of
                        Just _ -> [checked_]
                        Nothing -> []

                    replaced = case find (\(bomKey, _, itemKey, op) -> entityKey itemEnt == itemKey && op == "rpl") asmPlan of
                        Just _ -> [checked_]
                        Nothing -> []

                    used = case find (\(bomKey, _, itemKey, op) -> entityKey itemEnt == itemKey && op == "use") asmPlan of
                        Just _ -> [checked_]
                        Nothing ->
                            if (null replaced && null assembled && (1 == (length $ items bomLine))) then
                                [checked_]
                            else
                                []
                                    
                    radioName = case mRepworkItemKey of
                        Just key -> (T.pack $ show $ fromSqlKey $ entityKey $ bomlineTmpl bomLine)
                            <> "-" <> (T.pack $ show $ fromSqlKey key)
                        Nothing -> T.pack $ show $ fromSqlKey $ entityKey $ bomlineTmpl bomLine
                        
                    radioValue = T.pack $ show $ fromSqlKey $ entityKey $ itemEnt

                    indentation = T.replicate level $ T.pack "----"

                td_ $ toHtml $ indentation <> (itemTypeName $ entityVal $ lineType bomLine)
                td_ $ toHtml $ indentation <> (bomlinetmplPosOnDrawing $ entityVal $ bomlineTmpl bomLine)
                td_ $
                    input_ ([type_ "radio",
                        name_ radioName,
                        value_ $ radioValue <> "use" ] <> used)
                td_ $
                    input_ ([type_ "radio",
                        name_ radioName,
                        value_ $ radioValue <> "asm" ] <> assembled)
                td_ $
                    input_ ([type_ "radio",
                        name_ radioName,
                        value_ $ radioValue <> "rpl" ] <> replaced)
                td_ $ toHtml $ T.pack $ show $ itemCode1C $ entityVal itemEnt
                td_ $ toHtml $ itemPn $ entityVal itemEnt
                td_ $ toHtml $ if (itemIsObsolete $ entityVal itemEnt) then "O"::T.Text else ""
                td_ $ toHtml $ indentation <> (itemDescription $ entityVal itemEnt)
                td_ $ toHtml $ itemUom $ entityVal itemEnt
                td_ $ toHtml $ (toFixed 2) $ (fromIntegral $ evalQty bomLine) / (itemConversion $ entityVal itemEnt)
                td_ $ a_ [href_ ("/stock?id=" <> (T.pack $ show $ fromSqlKey $ entityKey itemEnt))] $ toHtml $ maybe "" (show . unSingle) $ head ohQty
                td_ $ a_ [href_ ("/stock?id=" <> (T.pack $ show $ fromSqlKey $ entityKey itemEnt))] $ toHtml $ maybe "" (show . unSingle) $ head ooQty

                case (length used, length assembled, length replaced) of
                    (_, 1, _) -> do
                        bomlines <- liftIO $ bomGenerator itemEnt pool 
                        mapM_ (\(lineNum, line) -> showBomLine (level+1) lineNum line) $ zip (iterate (+1) lineNum) bomlines
                    (_, _, 1) -> do
                        candidates <- liftIO $ searchReplacement itemEnt pool
                        mapM_ (showBomItem (level+1) lineNum bomLine (Just $ entityKey itemEnt)) candidates
                    (_, _, _) -> toHtml (""::T.Text)
                )
                    (if (odd lineNum) then [class_ "selected"] else [])

            showBomLine level lineNum bomLine =
                if evalQty bomLine /= 0 then
                    if (length $ items bomLine) > 0 then
                        mapM_ (showBomItem level lineNum bomLine Nothing) $ items bomLine 
                    else tr_ $ do
                        td_ $ toHtml $ itemTypeName $ entityVal $ lineType bomLine
                        td_ $ toHtml $ bomlinetmplPosOnDrawing $ entityVal $ bomlineTmpl bomLine
                else toHtml (""::T.Text) --mempty


                

        bomlines <- liftIO $ bomGenerator itemEnt pool


        h1_ $ toHtml $ (T.pack $ show $ itemCode1C item) <> "-" <> (itemPn item) <> "-" <> (itemDescription item)
        h2_ $ "Prepare assembling job"
        h3_ $ a_ [href_ $ "/checkBomBuild?item=" <> (T.pack $ show $ fromSqlKey $ entityKey itemEnt)] "Check BOM build"
        form_ [formaction_ "GET"] $ do
            input_ [type_ "hidden", name_ "item", value_ $ T.pack $ show $ fromSqlKey $ entityKey itemEnt]
            button_ [type_ "submit"] $ toHtml ("Update"::T.Text)
            button_ [type_ "reset"] $ toHtml ("Reset"::T.Text)
            button_ [type_ "submit", name_ "print" ] $ toHtml ("Result"::T.Text)
            table_ $ do
                tr_ $ do
                    th_ "Type"
                    th_ "Drawing Pos"
                    th_ "Use"
                    th_ "Asm"
                    th_ "Rpl"
                    th_ "Code 1C"
                    th_ "PN"
                    th_ "Obs."
                    th_ "Description"
                    th_ "UOM"
                    th_ "Qty required"
                    th_ "On Hand#"
                    th_ "On Order#"
                tbody_ $ mapM_ (\(lineNum, line) -> showBomLine 0 lineNum line) $ zip (iterate (+1) 0) bomlines

assemblingJobPrint :: Entity Item -> [(Key Bomlinetmpl, Maybe (Key Item),  Key Item, T.Text)] -> ConnectionPool -> Bool -> HtmlT IO ()
assemblingJobPrint itemEnt asmPlan pool isAuth = VL.templatePage2 body where
    body = do 
        let item = entityVal itemEnt
            itemKey = entityKey itemEnt
            mBomtmplKey = itemBomtmpl item

            showBomItem lineNum bomLine mRepworkItemKey itemsEnt2 = 

                case find (\(bomKey, _, _, op) -> bomKey == (entityKey $ bomlineTmpl bomLine) && op == "use") asmPlan of
                    Just (_, _, usedItemKey, _) -> do
                        ohQty :: [Maybe (Single Double)] <- liftIO $ runSqlPool (rawSql stockOnHandSql [toPersistValue usedItemKey]) pool
                        usedItem :: Item <- liftIO $ runSqlPool (getJust usedItemKey) pool
                        let usedItemEnt = Entity usedItemKey usedItem
                        tr_ $ do                    
                            td_ $ toHtml $ itemTypeName $ entityVal $ lineType bomLine
                            td_ $ toHtml $ bomlinetmplPosOnDrawing $ entityVal $ bomlineTmpl bomLine
                            td_ $ toHtml $ T.pack $ show $ itemCode1C $ entityVal usedItemEnt
                            td_ $ toHtml $ itemPn $ entityVal usedItemEnt
                            td_ $ toHtml $ itemDescription $ entityVal usedItemEnt
                            td_ $ toHtml $ itemUom $ entityVal usedItemEnt
                            td_ $ toHtml $ (toFixed 2) $ (fromIntegral $ evalQty bomLine) / (itemConversion $ entityVal usedItemEnt)
                            td_ $ toHtml $ maybe "" (show . unSingle) $ head ohQty

                    Nothing -> case find (\(bomKey, _, _, op) -> bomKey == (entityKey $ bomlineTmpl bomLine) && op == "asm") asmPlan of
                        Just (_, _, assembledItemKey, _) -> do
                                assembledItem :: Item <- liftIO $ runSqlPool (getJust assembledItemKey) pool
                                let assembledItemEnt = Entity assembledItemKey assembledItem
                                bomlines <- liftIO $ bomGenerator assembledItemEnt pool 
                                mapM_ (\(lineNum, line) -> showBomLine lineNum line) $ zip (iterate (+1) lineNum) bomlines
                        Nothing -> tr_ $ do
                            td_ $ toHtml $ itemTypeName $ entityVal $ lineType bomLine
                            td_ $ toHtml $ bomlinetmplPosOnDrawing $ entityVal $ bomlineTmpl bomLine


            showBomLine lineNum bomLine =
                if evalQty bomLine /= 0 then
                    showBomItem lineNum bomLine Nothing $ items bomLine 
                else toHtml (""::T.Text)


        bomlines <- liftIO $ bomGenerator itemEnt pool

        h1_ $ toHtml $ (T.pack $ show $ itemCode1C item) <> "-" <> (itemPn item) <> "-" <> (itemDescription item)
        h2_ $ "Assembling job"
        table_ $ do
            thead_ $ tr_ $ do
                th_ "Type"
                th_ "Drawing Pos"
                th_ "Code 1C"
                th_ "PN"
                th_ "Description"
                th_ "UOM"
                th_ "Qty required"
                th_ "On Hand#"
            tbody_ $ mapM_ (\(lineNum, line) -> showBomLine lineNum line) $ zip (iterate (+1) 0) bomlines


bomTmplSql :: T.Text
bomTmplSql = [r|
    SELECT ??, ??, ??
    FROM bomlinetmpl
    INNER JOIN item_type
    ON item_type.id = bomlinetmpl.childtype
    LEFT JOIN propertyfilter
    ON propertyfilter.tmpl_line = bomlinetmpl.id
    WHERE bomlinetmpl.tmpl = ?
    ORDER BY item_type.name ASC, bomlinetmpl.id ASC
|]

itemsForBomLineSql :: T.Text
itemsForBomLineSql = [r|
    SELECT ??
    FROM item_bom_link
    INNER JOIN item
    ON item.id = item_bom_link.item
    WHERE item_bom_link.bomline = ? 
|]

itemPropertiesExtSql :: T.Text
itemPropertiesExtSql = [r|
    SELECT ??
    FROM item_property
    WHERE item_property.item = ? 
|]

itemsForFilteringSql :: T.Text
itemsForFilteringSql = [r|
    SELECT ??, ??
    FROM item
    INNER JOIN item_property
    ON item_property.item = item.id
    INNER JOIN plan_item
    ON plan_item.id = item.plan_item
    WHERE plan_item.item_type = ?
    ORDER BY item.id ASC
|]

stockOnHandSql :: T.Text
stockOnHandSql = [r|
    SELECT sum(oh.qty) 
    FROM on_hand oh
    INNER JOIN stock_upload u
    ON u.id = oh.upload
    WHERE u.date IN (SELECT max(date) FROM stock_upload)
            AND oh.item = ? AND oh.status like 'Ready'
|]
stockOnOrderSql :: T.Text
stockOnOrderSql = [r|
    SELECT sum(oo.qty) 
    FROM on_order oo
    INNER JOIN stock_upload u
    ON u.id = oo.upload
    WHERE u.date IN (SELECT max(date) FROM stock_upload)
            AND oo.item = ?
|]
