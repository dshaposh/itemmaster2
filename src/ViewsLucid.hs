{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module ViewsLucid where

import GHC.Int
import Data.Monoid ((<>))
import qualified Data.Text as T
import Data.Double.Conversion.Text (toFixed)
import Data.Monoid (mempty)
import Lucid.Base
import Lucid.Html5
import Database.Persist.Postgresql

import Types
import PropertyType
import Data.Maybe (fromMaybe)

templatePage2 :: HtmlT IO () -> HtmlT IO ()
templatePage2 html =
    doctypehtml_ $ do
        head_ $ do
            title_ "Itemmaster"
            link_ [rel_ "stylesheet", type_ "text/css", href_ "style.css"]
        body_ $ do
            nav_ $ do
                div_ $ do
                    a_ [href_ "/"] "Home"
                    a_ [href_ "/searchItems"] "Search 1C Items"
                    a_ [href_ "/checkCodes1C"] "Check 1C Codes"
                    a_ [href_ "/stockuploads"] "Uploads"
                    a_  [href_ "/properties"] "Properties"
                    if not False then a_ [href_ "/login"] "LOGIN" else mempty
            section_ html 
            footer_ $ do
                div_ "Itemmaster"

templatePage :: Html () -> Html ()
templatePage html =
    doctypehtml_ $ do
        head_ $ do
            title_ "Itemmaster"
            link_ [rel_ "stylesheet", type_ "text/css", href_ "style.css"]
        body_ $ do
            nav_ $ do
                div_ $ do
                    a_ [href_ "/"] "Home"
                    a_ [href_ "/searchItems"] "Search 1C Items"
                    a_ [href_ "/checkCodes1C"] "Check 1C Codes"
                    a_ [href_ "/stockuploads"] "Uploads"
                    a_ [href_ "/properties"] "Properties"
                    if not False then a_ [href_ "/login"] "LOGIN" else mempty
            section_ html 
            footer_ $ do
                div_ "Itemmaster"

propertiesPage :: [PropertyType] -> HtmlT IO ()
propertiesPage propertyTypes = templatePage2 body where 
    body = do
        h1_ "Items properties"
        table_ $ do
            tr_ $ do
                th_ "id"
                th_ "Name"
            tbody_ $ mapM_ propertyView propertyTypes
        where
            propertyView property = 
                tr_ $ do 
                    td_ $ toHtml $ show property
                    td_ $ toHtml $ show property
                    toHtml (""::T.Text)


uploadsPage :: [(Entity StockUpload, Single Int64, Single Int64)] -> Bool -> Html ()
uploadsPage uploads isAuth = do
    h1_ "Stock uploads"
    if isAuth
        then a_ [href_ "/upload"] $ "Upload data"
        else mempty
    form_ $ do
        table_ $ do
            tr_ $ do
                th_ "Date"
                th_ "On order records #"
                th_ "On hand records #"
                if isAuth
                    then th_ "Action"
                    else mempty
            tbody_ $ mapM_ uploadView uploads
    where
        uploadView (entity, ooCount, ohCount)= 
            tr_ $ do 
                td_ $ toHtml $ show $ stockUploadDate $ entityVal entity
                td_ $ toHtml $ show $ unSingle ooCount
                td_ $ toHtml $ show $ unSingle ohCount
                if isAuth
                    then
                        td_ $ button_
                            [ type_ "submit"
                            , formmethod_ "POST"
                            , name_ "idToDelete"
                            , value_ (T.pack $ show $ fromSqlKey $ entityKey entity)
                            ]
                            "D"
                    else
                        toHtml (""::String) -- cannot change to mempty. Why???
--                        mempty

groupPage :: T.Text -> [(Entity ItemType, Single Int64, Single Int64)] -> Bool -> Html ()
groupPage groupId itemTypes isAuth = do
    h1_ $ toHtml $ "Group " ++ (getGroupName groupId) 
    if isAuth
        then div_ $ a_ [href_ ("aditType?group=" <> groupId)] "Add subgroup"
        else mempty
    form_ $ do
        table_ $ do
            tr_ $ do
                th_ "id"
                th_ "Name"
                th_ "UOM"
                th_ "Recuperation"
                th_ "Items"
                th_ "1C items"
                th_ "Bom templates"
                if isAuth then th_ "Action" else mempty
            tbody_ $ mapM_ itemTypeLine itemTypes
    where
        itemTypeLine (itemType, piQty, iQty) =
            tr_ $ do 
                td_ (if isAuth
                    then a_ [href_ ("/aditType?id=" <> key)] $ toHtml key
                    else toHtml key)
                td_ $ a_ [href_ ("/planItems?type=" <> key)] $ (toHtml . itemTypeName . entityVal) itemType 
                td_ $ (toHtml . itemTypeUom . entityVal) itemType 
                td_ $ (toHtml . (toFixed 2) . itemTypeRecuperation . entityVal) itemType 
                td_ $ toHtml $ show $ unSingle piQty
                td_ $ a_ [href_ ("/items?type=" <> key)] $ (toHtml $ show $ unSingle iQty) 
                td_ $ a_ [href_ ("/bomTemplates?type=" <> key)] "TBD"
                if isAuth
                    then
                        td_ $ button_
                            [ type_ "submit"
                            , formmethod_ "POST"
                            , formaction_ "deleteType"
                            , name_ "id"
                            , value_ key
                            ]
                            "D"
                    else
                        toHtml (""::String) -- cannot change to mempty. Why???
                        --mempty
            where  key = T.pack $ show (fromSqlKey $ entityKey itemType)


selectHtml :: String -> [(String, String)] -> T.Text -> Html ()
selectHtml tagName options selItem =
    select_ [ name_ $ T.pack tagName] $ mapM_ optionTag ((""::String, ""::String) : options) 
    where optionTag (descr, optId) = with
            option_ 
            ([ value_ $ T.pack optId ]
                <> (if selItem == T.pack optId then [ selected_ "selected" ] else []))
            $ toHtml descr 

selectHtml2 :: [Attribute] -> [(T.Text, T.Text)] -> T.Text -> HtmlT IO ()
selectHtml2 attributes options selItem = select_ attributes
    $ mapM_ optionTag ((""::T.Text, ""::T.Text) : options) 
    where optionTag (descr, optId) = with
            option_ 
            ([ value_ optId ]
                <> (if selItem == optId then [ selected_ "selected" ] else []))
            $ toHtml descr 

aditTypePage :: Either T.Text (Entity ItemType) -> Html ()
aditTypePage iniVal = do
    h1_ $ toHtml ((either (const "Add") (const "Edit") iniVal) <> " Item Type"::T.Text)
    form_ [ method_ "POST", action_ "/aditType" ] $ do
        fieldset_ $ do
            input_ [ type_ "hidden", name_ "id", value_ $ either (const "") (T.pack . show . fromSqlKey . entityKey) iniVal ]
            div_ [ class_ "line" ] $ do
                label_ "Name"
                input_ [ type_ "text" , name_ "name",
                        value_ $ either (const "") (itemTypeName . entityVal) iniVal, required_ "" ]

            div_ [ class_ "line" ] $ do
                label_ "Group"
                selectHtml "group" groups $ either id (itemTypeGroup . entityVal) iniVal 

            div_ [ class_ "line" ] $ do
                label_  "UOM"
                selectHtml "uom" uoms $ either id (itemTypeUom . entityVal) iniVal 

            div_ [ class_ "line" ] $ do
                label_ $ "Recuperation"
                input_ [ type_ "number", name_ "recuperation", step_ "any", required_ "",
                        value_ $ either (const "") ((toFixed 2) . itemTypeRecuperation . entityVal) iniVal ]

            div_ [ class_ "line" ] $ do
                label_ "Customs Code"
                input_ [ type_ "text", name_ "custcode",
                        value_ $ either (const "") (itemTypeCustomsCode . entityVal) iniVal ]

            div_ [ class_ "line" ] $ do
                label_ "Customs Duty"
                input_ [ type_ "number", name_ "custduty", step_ "any", required_ "",
                        value_ $ either (const "") ((toFixed 2) . itemTypeCustomsDuty . entityVal) iniVal ]

            div_ [ class_ "line" ] $ do
                label_ "Repl/RW rule"
                selectHtml "repworkrule"
                    (fmap (\a -> (show a, show a)) allRepworkRules)
                    $ either (const "") (\a -> maybe "" (T.pack . show) (itemTypeRepworkRule $ entityVal a)) iniVal

            div_ [ class_ "line" ] $ do
                label_ "Note"
                input_
                    [ type_ "text"
                    , name_ "note"
                    ,value_ $ either (const "") (itemTypeNote . entityVal) iniVal
                    ]

            div_ [ class_ "buttons" ] $ do
                button_ "Save"
                button_ [type_ "reset"] "Reset"
